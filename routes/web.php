<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware'=>'HtmlMinifier'], function(){ 
  
    Route::get('/', function () {
        return view('tienda');
    });
    Route::get('/producto/{slug}','Products\IndexController@producto')->name('producto');

    Route::get('/finalizar-compra', function () {
        return view('finalizar');
    });
    Route::get('/fin-compra', function () {
        return view('final');
    });
    Route::get('/fin-compra/{token}', function () {
        return view('final');
    });
    Route::get('/pedido/{token}', function () {
        return view('pedido');
    });
    Route::get('/orden/{num_orden}', function () {
        return view('orden');
    });
  
  });

  Route::get('sitemap.xml','SitemapController@index');

