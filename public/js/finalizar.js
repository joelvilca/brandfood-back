const BASE_URL = "https://estaraqui.cl/backend/public/api";
new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data() {
        return {
            isCuponAply: false,
            errorCupon: "",
            isCupon: false,
            cupon: "",
            cupon_id: "",
            descuento: 0,
            dialogVisible: false,
            messages: "",
            errorITems: [],
            email: "",
            courier_item: "",
            courier_id: "",
            codigo_courier: "",
            e6: 1,
            loading: false,
            loading1: false,
            loading2: false,
            pagos: ["Tarjeta de Débito / Crédito", "Transferencia Bancaria"],
            pago: "",
            form: {
                nombre: "",
                apellidos: "",
                telefono: "",
                celular: "",
                direccion: "",
                ciudad: "",
                notas: "",
            },
            error: {
                nombre: false,
            },
            formCart: {
                cupon: "",
                items: [],
                ciudad_origen_id: 175,
                ciudad_destino_id: "",
            },
            courier: [],
            cotizacion: true,
            random: Math.floor(Math.random() * 1000000000) + 1,
            url: "",
            token: "",
            ready: true,
            webpayPlus: true,
            shopping: [],
            ciudades: [],
            payed: {},
        };
    },
    computed: {},
    mounted() {
        this.getCiudades();
        if (this.fecha != null) {
            let fecha2 = new Date();
            let fecha1 = new Date(this.fecha);
            let resta = fecha2.getTime() - fecha1.getTime();
            if (Math.round(resta / (1000 * 60 * 60)) > 2) {
                let shop = [];
                this.removeShopping(shop);
            }
        }
        this.shopping =
            JSON.parse(localStorage.getItem("shopping")) != null
                ? JSON.parse(localStorage.getItem("shopping"))
                : [];
        if (!this.shopping.length) {
            window.location.replace(`/`);
        }
    },
    methods: {
        async searchForEmail() {
            let email = this.email;
            const res = await axios.get(
                //`https://patagoniablend.cl/patagonia_blend/backend/public/api/clientes/search?email=${email}`
                `${BASE_URL}/clientes/search?email=${email}`
            );
            this.form.nombre = res.data.data.nombre;
            this.form.apellidos = res.data.data.apellidos;
            this.form.telefono = res.data.data.telefono;
            this.form.direccion = res.data.data.direccion.direccion;
            this.form.ciudad = res.data.data.direccion.ciudad_id;
        },
        home() {
            window.location.replace(`/productos`);
        },
        formatMoney(numero) {
            return new Intl.NumberFormat(["ban", "id"]).format(numero);
        },
        async getCiudades() {
            try {
                const res = await axios.get(
                    //`https://patagoniablend.cl/patagonia_blend/backend/public/api/paises/1/ciudades`
                    `${BASE_URL}/paises/1/ciudades`
                );
                this.ciudades = res.data.data;
            } catch (error) {
                console.log(error);
            }
        },
        getPrecioTotal() {
            let sum = 0;
            console.log(this.shopping);
            for (let i = 0; i < this.shopping.length; i++) {
                sum = sum + this.shopping[i].precio * this.shopping[i].cantidad;
            }
            return sum;
        },
        addCount(id, value) {
            let shop = this.shopping.find((item) => item.id === id);
            shop.total = shop.precio * value;
            this.saveShopping(this.shopping);
        },
        saveShopping(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("shopping", parsed);
        },
        async submitCotizacion() {
            this.$refs.form.validate();
            if (this.$refs.form.validate() === false) return;
            this.loading = true;
            this.formCart.items = [];
            for (let i = 0; i < this.shopping.length; i++) {
                this.formCart.items.push({
                    item_id: this.shopping[i].id,
                    cantidad: this.shopping[i].cantidad,
                    // cantidad: 10,
                });
            }
            this.formCart.ciudad_destino_id = this.form.ciudad;
            try {
                const response = await axios.post(
                    //`https://patagoniablend.cl/patagonia_blend/backend/public/api/cotizaciones`,
                    `${BASE_URL}/cotizaciones`,
                    this.formCart
                );
                this.courier = response.data.data;
                this.e6 = 2;
                document.getElementById("step").scrollIntoView({
                    behavior: "smooth",
                });
                this.loading = false;
            } catch (error) {
                this.loading = false;
            }
        },
        pagar() {
            if (this.pago === "Transferencia Bancaria") {
                this.addPayLinkify();
            } else {
                this.addPay();
            }
        },
        addCourier(item) {
            this.courier_item = item;
        },
        _onBack() {
            this.e6 = 1;
            this.courier_id = "";
            this.courier_item = "";
        },
        async addPay() {
            try {
                this.loading2 = true;
                this.loading1 = true;
                const data = {
                    nombre: this.form.nombre,
                    apellidos: this.form.apellidos,
                    email: this.email,
                    telefono: this.form.telefono,
                    celular: this.form.celular,
                    direccion: this.form.direccion,
                    notas: this.form.notas,
                    ciudad_destino_id: this.formCart.ciudad_destino_id,
                    ciudad_origen_id: this.formCart.ciudad_origen_id,
                    despacho: this.courier_item.costo,
                    courier_id: this.courier_item.courier_id,
                    courier_codigo: this.courier_item.codigo_courier,
                    courier_nombre: this.courier_item.nombre_courier,
                    courier_descripcion: this.courier_item.descripcion,
                    items: this.formCart.items,
                    cupon_id: this.cupon_id,
                };
                const res = await axios.post(
                    //`https://patagoniablend.cl/patagonia_blend/backend/public/api/carros/transbank`,
                    `${BASE_URL}/carros/transbank`,
                    data
                );
                this.payed = res.data.data;
                this.metodoForm();
                this.loading1 = false;
                this.loading2 = false;
            } catch (error) {
                this.loading1 = false;
                this.loading2 = false;
                if (error.response.status === 422) {
                    this.errorITems = error.response.data.errors.items;
                    this.messages = error.response.data.messages;
                    this.dialogVisible = true;
                }
            }
        },
        async addPayLinkify() {
            try {
                this.loading2 = true;
                const data = {
                    nombre: this.form.nombre,
                    apellidos: this.form.apellidos,
                    email: this.email,
                    telefono: this.form.telefono,
                    celular: this.form.celular,
                    direccion: this.form.direccion,
                    notas: this.form.notas,
                    ciudad_destino_id: this.formCart.ciudad_destino_id,
                    ciudad_origen_id: this.formCart.ciudad_origen_id,
                    despacho: this.courier_item.costo,
                    courier_id: this.courier_item.courier_id,
                    courier_codigo: this.courier_item.codigo_courier,
                    courier_nombre: this.courier_item.nombre_courier,
                    courier_descripcion: this.courier_item.descripcion,
                    items: this.formCart.items,
                    cupon_id: this.cupon_id,
                };
                const res = await axios.post(
                    //`https://patagoniablend.cl/patagonia_blend/backend/public/api/carros/linkify`,
                    `${BASE_URL}/carros/linkify`,
                    data
                );
                this.saveToken(res.data.data.token);
                window.open(res.data.data.transbank_url, "_self");
                this.loading2 = false;
            } catch (error) {
                this.loading2 = false;
                if (error.response.status === 422) {
                    this.errorITems = error.response.data.errors.items;
                    this.messages = error.response.data.messages;
                    this.dialogVisible = true;
                }
            }
        },
        metodoForm() {
            this.saveToken(this.payed.transbank_token);
            this.token = this.payed.transbank_token;
            window.open(
                //`https://patagoniablend.cl/patagonia_blend/backend/public/api/transbank/redireccionar?token=${this.token}&url=${this.payed.transbank_url}`,
                `${BASE_URL}/transbank/redireccionar?token=${this.token}&url=${this.payed.transbank_url}`,
                "_self"
            );
            //document.getElementById('webpay-form-' + this.random).submit()
        },
        saveToken(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("tokenTransbank", parsed);
        },
        handleClose() {
            this.dialogVisible = false;
            // this.$router.push({ name: "productos" });
        },
        async validCupon() {
            try {
                const res = await this.getCupon({
                    codigo: this.cupon,
                    sub_total: this.getPrecioTotal(),
                });
                const cupon = res.data.data;
                const valor = cupon.monto_descuento;
                this.descuento = valor;
                this.cupon_id = cupon.id;
                this.isCuponAply = true;
                this.$message.success("Cupón aplicado exitosamente");
            } catch (error) {
                this.errorCupon = error.response.data.message;
            }
        },
        getCupon(payload = {}) {
            const params = payload.params || {};
            const codigo = payload.codigo;
            const subtotal = payload.sub_total;
            return new Promise((resolve, reject) => {
                axios({
                    //url: `https://patagoniablend.cl/patagonia_blend/backend/public/api/cupones/validar?codigo=${codigo}&subtotal=${subtotal}`,
                    url: `${BASE_URL}/cupones/validar?codigo=${codigo}&subtotal=${subtotal}`,
                    method: "get",
                    params,
                })
                    .then((response) => resolve(response))
                    .catch((error) => reject(error));
            });
        },
        deleteCupon() {
            this.descuento = 0;
            this.cupon = "";
            this.isCuponAply = false;
            this.cupon_id = "";
            this.isCupon = false;
        },
    },
});
