const BASE_URL = "https://estaraqui.cl/backend/public/api";
new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data() {
        return {
            shopping: [],
            loading: false,
            loading1: false,
            form: {
                nombre: "",
                telefono: "",
                celular: "9536622",
                direccion: "",
                ciudad: "",
            },
            error: {
                nombre: false,
            },
            formCart: {
                items: [],
                ciudad_origen_id: 175,
                ciudad_destino_id: "",
            },
            courier: [],
            cotizacion: true,
            finCompra: {},
            tokenTransbank: "",
            compra: "",
            fecha: "",
            etapas: "",
            messages: [
                {
                    from: "You",
                    message: `Sure, I'll see you later.`,
                    time: "10:42am",
                    color: "deep-purple lighten-1",
                },
                {
                    from: "John Doe",
                    message: "Yeah, sure. Does 1:00pm work?",
                    time: "10:37am",
                    color: "green",
                },
                {
                    from: "You",
                    message: "Did you still want to grab lunch today?",
                    time: "9:47am",
                    color: "deep-purple lighten-1",
                },
            ],
        };
    },
    async mounted() {
        await this.getCompra();
    },
    methods: {
        formatMoney(numero) {
            return new Intl.NumberFormat(["ban", "id"]).format(numero);
        },
        async getCompra() {
            try {
                let pageURL = window.location.href;
                let token = pageURL.substr(pageURL.lastIndexOf("/") + 1);
                const response = await axios(
                    `${BASE_URL}/carros/buscar?token=${token}`
                );
                this.compra = response.data.data;
                const res = await axios(
                    `${BASE_URL}/carros/${this.compra.numero_orden}/etapas`
                );
                this.etapas = res.data.data;
            } catch (error) {
                console.log(error);
            }
        },
        getPrecioTotal() {
            let sum = 0;
            for (let i = 0; i < this.shopping.length; i++) {
                sum = sum + this.shopping[i].total;
            }
            return sum;
        },
    },
});
