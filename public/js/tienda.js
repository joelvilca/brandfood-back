const BASE_URL = "https://estaraqui.cl/backend/public/api";
new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data() {
        return {
            drawer: false,
            num: 1,
            direction: "rtl",
            loading: false,
            loading1: false,
            message: "Hola",
            productos: [],
            search: "",
            productosTotal: [],
            categoria: "",
            buscar: "",
            limit: 9,
            id: "",
            btn: true,
            total: 0,
            totalProduct: 0,
            img: "",
            selectedCat: [],
            categorias: [],
            shopping: [],
            isMobile: false,
            dialogVisible: false,
            catVisible: false,
            items: [],
            shops: [],
            producto: "",
            searchDialog: false,
            producto_imagen: "",
            buscarProducto: "",
            fecha: "",
            snackbar: false,
            color: "",
            timeout: 1000,
            texto: "",
        };
    },
    computed: {
        datos() {
            if (this.search != "") {
                this.loading = true;
                setTimeout(() => {
                    this.loading = false;
                }, 1000);
                this.total = 0;
                if (this.categoria) {
                    return this.productosTotal.filter((dato) => {
                        return dato.busqueda
                            .toLowerCase()
                            .includes(
                                this.eliminarDiacriticos(
                                    this.search.toLowerCase()
                                )
                            );
                    });
                }
                return this.productosTotal.filter((dato) => {
                    return dato.busqueda
                        .toLowerCase()
                        .includes(
                            this.eliminarDiacriticos(this.search.toLowerCase())
                        );
                });
            }
            this.total = this.productosTotal.length;
            return this.productos;
        },
    },
    beforeDestroy() {
        if (typeof window === "undefined") return;
        window.removeEventListener("resize", this.onResize, { passive: true });
    },
    async mounted() {
        this.onResize();
        window.addEventListener("resize", this.onResize, { passive: true });
        this.shopping =
            JSON.parse(localStorage.getItem("shopping")) != null
                ? JSON.parse(localStorage.getItem("shopping"))
                : [];
        this.productosTotal =
            JSON.parse(localStorage.getItem("productosTotal")) != null
                ? JSON.parse(localStorage.getItem("productosTotal"))
                : [];
        this.categorias =
            JSON.parse(localStorage.getItem("categorias")) != null
                ? JSON.parse(localStorage.getItem("categorias"))
                : [];
        this.fecha =
            localStorage.getItem("fecha") != null
                ? localStorage.getItem("fecha")
                : "";
        if (!this.categorias.length) {
            this.saveFecha();
            this.getCategorias();
        } else {
            let fecha2 = new Date();
            let fecha1 = new Date(this.fecha);
            let resta = fecha2.getTime() - fecha1.getTime();
            console.log(Math.round((resta % (1000 * 60 * 60)) / (1000 * 60)));
            if (
                Math.round((resta % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) >
                1
            ) {
                this.getCategorias();
                this.shopping = [];
                this.saveShopping(this.shopping);
            }
        }
        if (!this.productosTotal.length) {
            this.saveFecha();
            this.getProductsTotal();
        } else {
            let fecha2 = new Date();
            let fecha1 = new Date(this.fecha);
            let resta = fecha2.getTime() - fecha1.getTime();
            if (
                Math.round((resta % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) >
                1
            ) {
                this.getProductsTotal();
                this.saveFecha();
            }
            this.total = this.productosTotal.length;
            this.totalProduct = this.total;
            this.productos = this.productosTotal.filter(
                (producto) => producto.etiquetas == "Destacado"
            );
            this.loading1 = false;
            this.loading = false;
        }
    },
    methods: {
        getProducts() {
            this.productos = this.productosTotal.filter(
                (producto) => producto.etiqueta === "Destacado"
            );
        },
        onResize() {
            this.isMobile = window.innerWidth < 600;
        },
        openDrawer() {
            this.drawer = true;
        },
        openDialog() {
            this.searchDialog = true;
        },
        closeSearch() {
            this.searchDialog = false;
        },
        saveFecha() {
            this.fecha = new Date();
            localStorage.setItem("fecha", this.fecha.toString());
            console.log(this.fecha);
        },
        searchProduct() {
            this.searchDialog = false;
            let slug = this.buscarProducto;
            window.location.replace(`/producto/${slug}`);
        },
        openCatMovil() {
            this.catVisible = true;
        },
        getPrecioTotal() {
            let sum = 0;
            for (let i = 0; i < this.shopping.length; i++) {
                sum = sum + this.shopping[i].precio * this.shopping[i].cantidad;
            }
            return sum;
        },
        async handleOpen(item) {
            try {
                let slug = item.slug;
                this.producto = item.nombre;
                this.producto_imagen = item.imagen_thumbnail_path;
                const res = await axios.get(
                    // `https://patagoniablend.cl/patagonia_blend/backend/public/api/productos/${slug}/items`
                    `${BASE_URL}/productos/${slug}/items`
                );
                this.items = res.data.data;
                this.dialogVisible = true;
            } catch (error) {
                console.log(error);
            }
        },
        handleClose() {
            this.dialogVisible = false;
        },
        formatMoney(numero) {
            return new Intl.NumberFormat(["ban", "id"]).format(numero);
        },
        removeShop(x) {
            this.shopping.splice(x, 1);
            this.saveShopping(this.shopping);
        },
        checkout() {
            window.fbq("track", "InitiateCheckout");
        },
        saveShopping(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("shopping", parsed);
        },
        saveProductosTotal(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("productosTotal", parsed);
        },
        saveCategorias(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("categorias", parsed);
        },
        addCountDrawer(item, ope) {
            if (ope === "plus") {
                item.cantidad++;
            } else {
                item.cantidad--;
            }
            let shop = this.shopping.find((dato) => dato.id === item.id);
            shop.cantidad = item.cantidad;
            shop.total = shop.precio * item.cantidad;
            console.log(this.shopping);
            this.saveShopping(this.shopping);
        },
        async getCategorias() {
            try {
                const response = await axios.get(
                    //"https://patagoniablend.cl/patagonia_blend/backend/public/api/categorias?estado=true"
                    `${BASE_URL}/categorias?estado=true`
                );
                this.categorias = response.data.data;
                this.saveCategorias(this.categorias);
            } catch (error) {
                console.log(error);
            }
        },
        getProductsTotal() {
            this.loading = true;
            axios
                .get(
                    // `https://patagoniablend.cl/patagonia_blend/backend/public/api/productos/general`
                    `${BASE_URL}/productos/general`
                )
                .then((result) => {
                    this.productosTotal = result.data.data;
                    this.saveProductosTotal(result.data.data);
                    this.total = this.productosTotal.length;
                    this.totalProduct = this.total;
                    this.productos = this.productosTotal.filter(
                        (producto) => producto.etiquetas == "Destacado"
                    );
                    this.loading1 = false;
                    this.loading = false;
                });
        },
        onBusqueda() {
            if (screen.width < 500) {
                window.scroll({
                    top: 75,
                    left: 0,
                    behavior: "smooth",
                });
            } else {
                document.getElementById("banner").scrollIntoView({
                    behavior: "smooth",
                });
            }
        },
        addCount(item, producto, ope) {
            if (ope === "plus") {
                item.cantidad++;
            } else {
                item.cantidad--;
            }
            if (this.shops.length > 0) {
                let shop = this.shops.find((shop) => shop.id === item.id);
                if (shop) {
                    if (item.cantidad > 0) {
                        shop.total = shop.precio * item.cantidad;
                        shop.cantidad = item.cantidad;
                    } else {
                        this.shops = this.shops.filter(
                            (item) => item.id != shop.id
                        );
                    }
                } else {
                    let data = {};
                    data = {
                        id: item.id,
                        nombre: item.nombre,
                        precio: item.precio_bruto,
                        total: item.precio_bruto * item.cantidad,
                        cantidad: item.cantidad,
                        producto: this.producto,
                        imagen: item.imagen_producto.imagen_thumbnail_path,
                        stock: item.stock_disponible,
                    };
                    if (data.cantidad > 0) {
                        this.shops.push(data);
                    }
                }
            } else {
                let data = {};
                data = {
                    id: item.id,
                    nombre: item.nombre,
                    precio: item.precio_bruto,
                    total: item.precio_bruto * item.cantidad,
                    cantidad: item.cantidad,
                    producto: this.producto,
                    imagen: item.imagen_producto.imagen_thumbnail_path,
                    stock: item.stock_disponible,
                };
                if (data.cantidad > 0) {
                    this.shops.push(data);
                }
            }
        },
        addCart(producto_id, producto_nombre, producto_precio) {
            if (this.shops.length === 0) {
                this.color = "orange";
                this.snackbar = true;
                this.texto = "Debe seleccionar una cantidad mayor a 0.";
                return;
            } else {
                window.fbq("track", "AddToCart", {
                    content_name: producto_nombre, // required property
                    content_ids: producto_id,
                    content_type: "product",
                    value: producto_precio,
                    currency: "CLP",
                });
                this.dialogVisible = false;
                if (this.shopping.length > 0) {
                    for (let i = 0; i < this.shops.length; i++) {
                        let shop = this.shopping.find(
                            (shop) => shop.id === this.shops[i].id
                        );
                        if (shop) {
                            if (this.shops[i].cantidad != 0) {
                                shop.cantidad = this.shops[i].cantidad;
                            } else {
                                const result = this.shopping.filter(
                                    (item) => item.id != shop.id
                                );
                                this.saveShopping(result);
                            }
                        } else {
                            if (this.shops[i].cantidad != 0) {
                                this.shopping.push(this.shops[i]);
                            }
                        }
                    }
                    if (this.shopping.length > 0) {
                        this.saveShopping(this.shopping);
                        this.color = "success";
                        this.snackbar = true;
                        this.texto = "Su producto fue agregado exitosamente";
                    } else {
                        this.color = "orange";
                        this.snackbar = true;
                        this.texto = "Debe seleccionar una cantidad mayor a 0.";
                    }
                } else {
                    for (let i = 0; i < this.shops.length; i++) {
                        if (this.shops[i].cantidad != 0) {
                            this.shopping.push(this.shops[i]);
                        }
                    }
                    if (this.shopping.length > 0) {
                        let datos = this.shopping;
                        this.saveShopping(datos);
                        this.color = "success";
                        this.snackbar = true;
                        this.texto =
                            "Se agrego exitosamente al carrito de compras!";
                        this.openDrawer();
                    } else {
                        this.color = "orange";
                        this.snackbar = true;
                        this.texto = "Debe seleccionar una cantidad mayor a 0.";
                        return;
                    }
                }
            }
        },
        saveShopping(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("shopping", parsed);
        },
        getFilter(id) {
            this.loading = true;
            if (id === "Quitar") {
                this.selectedCat = [];
                this.productos = this.productosTotal.filter(
                    (producto) => producto.etiquetas == "Destacado"
                );
                this.catVisible = false;
                this.categoria = "";
                this.search = "";
            } else if (id === "Todos") {
                this.selectedCat = [];
                this.productos = this.productosTotal;
                this.categoria = "";
                this.search = "";
            } else {
                this.search = "";
                this.categoria = id;
                this.productos = [];
                let filters = [];
                var indice = this.selectedCat.indexOf(id);
                if (indice == -1) {
                    this.selectedCat.push(id);
                } else {
                    this.selectedCat.splice(indice, 1);
                    if (this.selectedCat.length == 0) {
                        this.getFilter("Quitar");
                        return;
                    }
                }

                for (let i = 0; i < this.selectedCat.length; i++) {
                    let datos = this.productosTotal.filter(
                        (producto) =>
                            producto.categoria.id === this.selectedCat[i]
                    );
                    filters = filters.concat(datos);
                    this.productos = filters;
                }

                this.btn = false;
            }
            setTimeout(() => {
                this.loading = false;
            }, 500);
        },
        eliminarDiacriticos(texto) {
            return texto.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        },
    },
});
