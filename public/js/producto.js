const BASE_URL = "https://estaraqui.cl/backend/public/api";
new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data() {
        return {
            drawer: false,
            shopping: [],
            productos: [],
            search: "",
            productosTotal: [],
            categoria: "",
            banner: "",
            shops: [],
            producto: "",
            id: "",
            btn: true,
            total: 0,
            totalProduct: 0,
            currentImage: 0,
            items: [],
            relacionados: [],
            isMobile: false,
            dialogVisible: false,
            nombre_item: "",
            descripcion_item: "",
            searchDialog: false,
            buscarProducto: "",
            imagen_item: "",
            tab: "",
            snackbar: false,
            color: "",
            timeout: 2000,
            texto: "",
            tab: null,
            fecha: "",
        };
    },
    beforeDestroy() {
        if (typeof window === "undefined") return;

        window.removeEventListener("resize", this.onResize, { passive: true });
    },
    async mounted() {
        this.onResize();
        window.addEventListener("resize", this.onResize, { passive: true });
        this.getItems();
        this.getProduct();
        this.productosTotal =
            JSON.parse(localStorage.getItem("productosTotal")) != null
                ? JSON.parse(localStorage.getItem("productosTotal"))
                : [];
        this.shopping =
            JSON.parse(localStorage.getItem("shopping")) != null
                ? JSON.parse(localStorage.getItem("shopping"))
                : [];
        this.fecha =
            localStorage.getItem("fecha") != null
                ? localStorage.getItem("fecha")
                : "";
        // para que cada 2 horas se vuelva a limpiar el shopping
        let fecha2 = new Date();
        let fecha1 = new Date(this.fecha);
        let resta = fecha2.getTime() - fecha1.getTime();
        if (
            Math.round((resta % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) > 1
        ) {
            this.shopping = [];
            this.saveShopping(this.shopping);
            this.getProductsTotal();
        } else if (!this.productosTotal.length) {
            this.getProductsTotal();
        }
    },
    methods: {
        async getProduct() {
            try {
                let pageURL = window.location.href;
                let slug = pageURL.substr(pageURL.lastIndexOf("/") + 1);
                const res = await axios.get(
                    //`https://patagoniablend.cl/patagonia_blend/backend/public/api/productos/${slug}`
                    `${BASE_URL}/productos/${slug}`
                );
                this.producto = res.data.data;
            } catch (error) {
                console.log(error);
            }
        },
        async getItems() {
            try {
                let pageURL = window.location.href;
                let slug = pageURL.substr(pageURL.lastIndexOf("/") + 1);
                const res = await axios.get(
                    // `https://patagoniablend.cl/patagonia_blend/backend/public/api/productos/${slug}/items`
                    `${BASE_URL}/productos/${slug}/items`
                );
                this.items = res.data.data;
            } catch (error) {
                console.log(error);
            }
        },
        onResize() {
            this.isMobile = window.innerWidth < 600;
        },
        formatMoney(numero) {
            return new Intl.NumberFormat(["ban", "id"]).format(numero);
        },
        searchProduct() {
            let slug = this.buscarProducto;
            window.location.replace(`/producto/${slug}`);
        },
        getProductsTotal() {
            this.loading = true;
            axios
                .get(
                    //`https://patagoniablend.cl/patagonia_blend/backend/public/api/productos/general`
                    `${BASE_URL}/productos/general`
                )
                .then((result) => {
                    this.productosTotal = result.data.data;
                });
        },
        addCountDrawer(item, ope) {
            if (ope === "plus") {
                item.cantidad++;
            } else {
                item.cantidad--;
            }
            let shop = this.shopping.find((dato) => dato.id === item.id);
            shop.cantidad = item.cantidad;
            shop.total = shop.precio * item.cantidad;
            this.saveShopping(this.shopping);
        },
        getPrecioTotal() {
            let sum = 0;
            for (let i = 0; i < this.shopping.length; i++) {
                sum = sum + this.shopping[i].precio * this.shopping[i].cantidad;
            }
            return sum;
        },
        handleOpen(item) {
            this.dialogVisible = true;
            this.nombre_item = item.nombre;
            this.descripcion_item = item.imagen.comentario;
            this.imagen_item = item.imagen.imagen_thumbnail_path;
        },
        handleClose() {
            this.dialogVisible = false;
        },
        openDrawer() {
            this.drawer = true;
        },
        handleCloseDrawer() {
            this.drawer = false;
        },
        openDialog() {
            this.searchDialog = true;
        },
        closeSearch() {
            this.searchDialog = false;
        },
        searchProduct() {
            this.searchDialog = false;
            let slug = this.buscarProducto;
            window.location.replace(`/producto/${slug}`);
        },
        checkout() {
            window.fbq("track", "InitiateCheckout");
        },
        removeShop(x) {
            this.shopping.splice(x, 1);
            this.saveShopping(this.shopping);
        },
        saveShopping(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("shopping", parsed);
        },
        addCount(item, producto, ope) {
            if (ope === "plus") {
                item.cantidad++;
            } else {
                item.cantidad--;
            }
            if (this.shops.length > 0) {
                let shop = this.shops.find((shop) => shop.id === item.id);
                if (shop) {
                    if (item.cantidad > 0) {
                        shop.total = shop.precio * item.cantidad;
                        shop.cantidad = item.cantidad;
                    } else {
                        this.shops = this.shops.filter(
                            (item) => item.id != shop.id
                        );
                    }
                } else {
                    let data = {};
                    data = {
                        id: item.id,
                        nombre: item.nombre,
                        precio: item.precio_bruto,
                        total: item.precio_bruto * item.cantidad,
                        cantidad: item.cantidad,
                        producto: producto.nombre,
                        imagen: item.imagen_producto.imagen_thumbnail_path,
                        stock: item.stock_disponible,
                    };
                    if (data.cantidad > 0) {
                        this.shops.push(data);
                    }
                }
            } else {
                let data = {};
                data = {
                    id: item.id,
                    nombre: item.nombre,
                    precio: item.precio_bruto,
                    total: item.precio_bruto * item.cantidad,
                    cantidad: item.cantidad,
                    producto: producto.nombre,
                    imagen: item.imagen_producto.imagen_thumbnail_path,
                    stock: item.stock_disponible,
                };
                if (data.cantidad > 0) {
                    this.shops.push(data);
                }
            }
        },
        addCart(producto_id, producto_precio, producto_nombre) {
            if (this.shops.length === 0) {
                this.color = "orange";
                this.snackbar = true;
                this.texto = "Debe seleccionar una cantidad mayor a 0.";
                this.return;
            } else {
                window.fbq("track", "AddToCart", {
                    content_name: producto_nombre, // required property
                    content_ids: producto_id,
                    content_type: "product",
                    value: producto_precio,
                    currency: "CLP",
                });
                if (this.shopping.length > 0) {
                    for (let i = 0; i < this.shops.length; i++) {
                        let shop = this.shopping.find(
                            (shop) => shop.id === this.shops[i].id
                        );
                        if (shop) {
                            if (this.shops[i].cantidad != 0) {
                                shop.cantidad = this.shops[i].cantidad;
                            } else {
                                const result = this.shopping.filter(
                                    (item) => item.id != shop.id
                                );
                                this.saveShopping(result);
                            }
                        } else {
                            if (this.shops[i].cantidad != 0) {
                                this.shopping.push(this.shops[i]);
                            }
                        }
                    }
                    if (this.shopping.length > 0) {
                        this.saveShopping(this.shopping);

                        this.color = "success";
                        this.snackbar = true;
                        this.texto = "Su producto fue agregado exitosamente";
                        this.openDrawer();
                    } else {
                        this.color = "orange";
                        this.snackbar = true;
                        this.texto = "Debe seleccionar una cantidad mayor a 0";
                    }
                } else {
                    for (let i = 0; i < this.shops.length; i++) {
                        if (this.shops[i].cantidad != 0) {
                            this.shopping.push(this.shops[i]);
                        }
                    }
                    if (this.shopping.length > 0) {
                        let datos = this.shopping;
                        this.saveShopping(datos);
                        this.color = "success";
                        this.snackbar = true;
                        this.texto =
                            "Se agrego exitosamente al carrito de compras!";
                        this.openDrawer();
                    } else {
                        this.color = "orange";
                        this.snackbar = true;
                        this.texto = "Debe seleccionar una cantidad mayor a 0.";
                        return;
                    }
                }
            }
        },
    },
});
