new Vue({
    el: "#carro",
    vuetify: new Vuetify(),
    data() {
        return {
            auth: false,
            dialogVisible: false,
            logo: "favicon.ico",
            color: "primary",
            cotizacion: 0,
            cantidad: 0,
            tablaNutricional: [],
            trazabilidad: [],
            informacionAdicional: [],
            caracteristicas: [],
            shops: [],
            nombre_item: "",
            descripcion_item: "",
            imagen_item: "",
            banner: "",
            image_desktop: "",
            image_mobil: "",
            type: "despacho",
            loading: false,
            relacionados: [],
            currentImage: 0,
            producto: {},
            drawer: true,
            fecha: "",
            items: [],
            shopping: [],
            logo: "favicon.ico",
            color: "primary",
            direction: "rtl",
            cotizacion: 0,
        };
    },
    async mounted() {
        this.shopping =
            JSON.parse(localStorage.getItem("shopping")) != null
                ? JSON.parse(localStorage.getItem("shopping"))
                : [];
        console.log("shoping", this.shopping.length);
    },
    methods: {
        formatMoney(numero) {
            return new Intl.NumberFormat(["ban", "id"]).format(numero);
        },
        removeShop(x) {
            this.shopping.splice(x, 1);
            this.saveShopping(this.shopping);
        },
        saveShopping(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("shopping", parsed);
        },
        countSet(item) {
            let shop = this.shopping.find((shop) => shop.id === item.id);
            if (!shop) {
                return 0;
            } else {
                return shop.cantidad;
            }
        },
        getPrecioTotal() {
            let sum = 0;
            for (let i = 0; i < this.shopping.length; i++) {
                sum = sum + this.shopping[i].precio * this.shopping[i].cantidad;
            }
            return sum;
        },
        handleClose() {
            this.dialogVisible = false;
        },
        handleOpen(item) {
            this.dialogVisible = true;
            this.nombre_item = item.nombre;
            this.descripcion_item = item.imagen.comentario;
            this.imagen_item = item.imagen.imagen_thumbnail_path;
        },
        openDrawer() {
            this.drawer = true;
        },
        handleCloseDrawer() {
            this.drawer = false;
        },
        addCountDrawer(item, ope) {
            if (ope === "plus") {
                console.log("plus");
                item.cantidad++;
            } else {
                console.log("less");
                item.cantidad--;
            }
            let shop = this.shopping.find((dato) => dato.id === item.id);
            shop.cantidad = item.cantidad;
            shop.total = shop.precio * item.cantidad;
            console.log(shopping);
            this.saveShopping(this.shopping);
        },
    },
});
