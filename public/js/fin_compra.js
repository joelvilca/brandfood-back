new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data() {
        return {
            shopping: [],
            loading: false,
            loading1: false,
            form: {
                nombre: "",
                telefono: "",
                celular: "9536622",
                direccion: "",
                ciudad: "",
            },
            error: {
                nombre: false,
            },
            formCart: {
                items: [],
                ciudad_origen_id: 175,
                ciudad_destino_id: "",
            },
            courier: [],
            cotizacion: true,
            finCompra: {},
            tokenTransbank: "",
            compra: "",
            fecha: "",
        };
    },
    async mounted() {
        this.tokenTransbank = JSON.parse(
            localStorage.getItem("tokenTransbank")
        );
        console.log(this.tokenTransbank);
        if (!this.tokenTransbank) {
            window.location.replace(`/`);
        }
        this.shopping =
            JSON.parse(localStorage.getItem("shopping")) != null
                ? JSON.parse(localStorage.getItem("shopping"))
                : [];
        await this.getCompra();
        if (this.compra.pago.estado_pago === "PAGADO") {
            let toke = "";
            let shops = [];
            this.saveShopping(shops);
            this.saveToken(toke);
            window.fbq("track", "Purchase", {
                content_type: "product",
                content_ids: this.compra.productos,
                value: this.compra.pago.monto_pagado,
                currency: "CLP",
            });
        }
    },
    methods: {
        formatMoney(numero) {
            return new Intl.NumberFormat(["ban", "id"]).format(numero);
        },
        async getCompra() {
            try {
                let token = this.tokenTransbank;
                const response = await axios(
                    `https://patagoniablend.cl/patagonia_blend/backend/public/api/carros/buscar?token=${token}`
                );
                this.compra = response.data.data;
            } catch (error) {
                console.log(error);
            }
        },
        saveShopping(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("shopping", parsed);
        },
        saveToken(payload) {
            const parsed = JSON.stringify(payload);
            localStorage.setItem("tokenTransbank", parsed);
        },
        getPrecioTotal() {
            let sum = 0;
            for (let i = 0; i < this.shopping.length; i++) {
                sum = sum + this.shopping[i].total;
            }
            return sum;
        },
    },
});
