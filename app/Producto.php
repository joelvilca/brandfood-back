<?php

namespace App;

use App\Helpers\File;
use App\Helpers\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Producto extends Model
{
    const ITEM = 'item';
    const TALLA = 'talla';
    const COLOR = 'color';

    const SERVICIO = 'servicio';

    const KEY_CACHE = 'productos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'SKU',
        'nombre',
        'descripcion_corta',
        'descripcion_larga',
        'estado',
        'slug',
        'tipo',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'estado' => 'boolean',
    ];

    /**
     * Set the product's slug.
     *
     * @param string $value
     *
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::of($value)->slug('-');
    }

    /**
     * get the product's key cache.
     *
     * @return void
     */
    public function getKeyCacheAttribute()
    {
        return 'producto_'.$this->slug;
    }

    public function categoria(): BelongsTo
    {
        return $this->belongsTo('App\Categoria');
    }

    public function atributos(): HasMany
    {
        return $this->hasMany('App\CampoProducto');
    }

    public function imagenes(): HasMany
    {
        return $this->hasMany('App\ImagenProducto');
    }

    public function items(): HasMany
    {
        return $this->hasMany('App\ItemProducto');
    }

    public function etiquetaProductos(): HasMany
    {
        return $this->hasMany('App\EtiquetaProducto');
    }

    public function tieneItems()
    {
        return $this->items()
                    ->exists();
    }

    public function tieneImagenes()
    {
        return $this->imagenes()
                    ->exists();
    }

    public function itemsActivos()
    {
        return $this->items()
                    ->where('estado', 1)
                    ->get();
    }

    public function itemsValidos()
    {
        return $this->items()
                    ->where('estado', 1)
                    ->where('precio_bruto', '>', 0)
                    ->whereHas('dimension', function ($query) {
                        return $query->where('peso', '>', 0);
                    })
                    ->get();
    }

    public function generarNombresEtiquetasActivas()
    {
        $etiquetas = collect();

        $etiquetasRelacionadas = $this->etiquetaProductos()
                                    ->whereDate('fecha_inicio', '<=', date('Y-m-d'))
                                    ->whereDate('fecha_termino', '>=', date('Y-m-d'))
                                    ->get();

        foreach ($etiquetasRelacionadas as $etiqueta) {
            $etiquetas->push($etiqueta->etiqueta);
        }

        return $etiquetas->implode('nombre', ', ');
    }

    public function obtenerImagenPrincipalFrontal()
    {
        if (!$this->tieneImagenes()) {
            return '';
        }

        $imagen = $this->imagenes()
                        ->where('tipo', ImagenProducto::PRINCIPAL_FRONTAL)
                        ->first();

        if (!$imagen) {
            return '';
        }

        return $imagen;
    }

    public function saveAttributes($atributos)
    {
        foreach ($atributos as $atributo) {
            $clasificacion = Clasificacion::findOrFail($atributo['clasificacion_id']);

            $atributo = CampoProducto::make([
                'nombre_campo' => $atributo['nombre_campo'],
                'valor_campo' => $atributo['valor_campo'],
                'mostrar' => $atributo['mostrar'],
                'tipo_campo' => $atributo['tipo_campo'],
            ]);

            $atributo->producto()->associate($this->id);
            $atributo->clasificacion()->associate($clasificacion->id);
            $atributo->save();
        }
    }

    public function obtenerNombresDeAtributos()
    {
        $nombres = collect();
        foreach ($this->atributos as $atributo) {
            $nombres->push($atributo->nombre_campo);
        }

        return $nombres;
    }

    public function obtenerProductosRelacionadosPorCategoria($categoria)
    {
        $productos = $categoria
                        ->productos
                        ->where('id', '!=', $this->id)
                        ->where('estado', 1);

        $productos->load('categoria');

        return $productos;
    }

    public function generarCadenaBusqueda()
    {
        $cadenaCompleta = Str::slug($this->nombre).' '.Str::slug($this->categoria->nombre);

        return str_replace('-', ' ', $cadenaCompleta);
    }

    public function obtenerCache()
    {
        Cache::get($this->key_cache);
    }

    public function eliminarCache()
    {
        Cache::forget($this->key_cache);
        Cache::forget(self::KEY_CACHE);
    }

    public function guardarCache($producto)
    {
        Cache::forever($this->key_cache, $producto);
    }

    public static function obtenerPorTipo()
    {
        $productos = Producto::with(['categoria', 'imagenes'])
                                ->where('estado', 1)
                                ->orderBy('categoria_id', 'ASC')
                                ->get();

        foreach ($productos as $producto) {
            $imagenFrontal = $producto->obtenerImagenPrincipalFrontal();

            $producto->imagen_url = $imagenFrontal ? $imagenFrontal->imagen_url : '';
            $producto->imagen_path = $imagenFrontal ? File::obtenerUrlCompleta($imagenFrontal->imagen_url) : '';

            $producto->imagen_thumbnail_url = $imagenFrontal ? $imagenFrontal->img_thumb_url : '';
            $producto->imagen_thumbnail_path = $imagenFrontal ? File::obtenerUrlCompleta($imagenFrontal->img_thumb_url) : '';

            $producto->busqueda = $producto->generarCadenaBusqueda();
            $producto->etiquetas = $producto->generarNombresEtiquetasActivas();
        }

        Cache::forever(self::KEY_CACHE, $productos);

        return $productos;
    }

    public function crearEnFacebook()
    {
        try {
            $access_token = 'EAADOatUA0XoBAIfZCm9IqZCFxIyXKfDF7KewyKMgHS53c1ZCbheVv9xiWO1e1s76yVUA0H6kTo4TUJYE0JpiDFVPgUZCMzlIl99A6Fc5pjtxIcpD70aQeepxedja9UcbPtdSHgLvCs5PPctbiHqUnZBhrDKCqHRhnWbRVYlp3zwir1DZBEBJh5';
            $pixel_id = '1079560299151625';
            $test_event_code = 'TEST7984';
            $catalogo_id = '462376344917964';

            $url = 'https://graph.facebook.com/v9.0/'.$catalogo_id.'/batch';

            $method = 'POST';

            $data = [
                'access_token' => $access_token,
  'requests' => [
   '0' => [
    'method' => 'CREATE',
    'retailer_id' => 'retailer-2',
        'data' => [
            'availability' => 'sin stock',
            'brand' => 'Nike',
            'category' => 't-shirts',
            'description' => 'product description',
            'image_url' => 'http://www.images.example.com/t-shirts/1.png',
    //   "name": "product name",
    //   "price": "10.00",
    //   "currency": "USD",
    //   "shipping": [
    //      {
    //         "country": "US",
    //         "region": "CA",
    //         "service": "service",
    //         "price_value": "10",
    //         "price_currency": "USD"
    //      }
    //   ],
    //   "condition": "new",
    //   "url":"http://www.images.example.com/t-shirts/1.png",
    //   "retailer_product_group_id": "product-group-1"
        ],
    ],
   ],
];

            $carroHubspot = Request::make($url, $method, $data);

            // $carro->actualizarDeal($carroHubspot->dealId);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
    }
}
