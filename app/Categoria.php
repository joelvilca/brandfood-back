<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    use SoftDeletes;

    const CAFE_TOSTADO = 'Café Tostado';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'nivel',
        'imagen_url',
        'imagen_path',
        'estado',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'estado' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    public function productos(): HasMany
    {
        return $this->hasMany('App\Producto');
    }

    public function scopeActivo($query, $estado)
    {
        return $query->where('estado', $estado);
    }

    public function esCafeTostado(): bool
    {
        return $this->nombre === self::CAFE_TOSTADO;
    }

    public function actualizarNivel($nivel)
    {
        $this->nivel = $nivel;
        $this->save();
    }
}
