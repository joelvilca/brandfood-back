<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Clasificacion extends Model
{
    /**
     * The table associated with the model.
     */
    protected $table = 'clasificaciones';

    /*
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'nombre',
        'estado',
        'descripcion',
   ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'estado' => 'boolean',
    ];

    public function atributos(): HasMany
    {
        return $this->hasMany('App\CampoProducto');
    }
}
