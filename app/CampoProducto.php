<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampoProducto extends Model
{
    use SoftDeletes;
    const INT = 'INT';
    const DOUBLE = 'DOUBLE';
    const TEXT = 'TEXT';
    const CURRENCY = 'CURRENCY';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre_campo',
        'valor_campo',
        'mostrar',
        'tipo_campo',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    public function producto(): BelongsTo
    {
        return $this->belongsTo('App\Producto');
    }

    public function clasificacion(): BelongsTo
    {
        return $this->belongsTo('App\Clasificacion');
    }
}
