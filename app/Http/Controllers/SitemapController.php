<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Producto;
use Illuminate\Http\Request;


class SitemapController extends Controller
{
    
    public function index(Request $r)
    {
       
        $productos = Producto::orderBy('id','asc')->where('estado', 1)->get();
        

        return response()->view('sitemap', compact('productos'))
          ->header('Content-Type', 'text/xml');

    }
}