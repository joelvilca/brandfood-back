<?php

namespace App\Http\Controllers\Products;

use App\Producto;
use App\ImagenProducto;
use App\Categoria;
use App\Clasificacion;
use App\ItemProducto;
use App\CampoProducto;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{
    const base_url = 'https://www.patagoniablend.cl/patagonia_blend/backend/public/storage/productos/imagenes/';

    public function __invoke()
    {
        
        // $products = Producto::all();

        // return ProductResource::collection($products);
    }

    public function producto($slug)
    {
        if (Cache::has('producto_'.$slug)) {
            $producto = Cache::get('producto_'.$slug);
        }else{
            $producto = Producto::
                            where('slug', $slug)
                            ->where('estado',1)
                            ->first();     
    
            if (isset($producto)) {
                $producto->guardarCache($producto);
            }
        }
        if (!$producto) {
            return abort(404);
        }
    //Obtener precio del 1er item de producto
        $item = ItemProducto::where('producto_id',$producto->id)
                            ->where('estado',1)
                            ->first();
        
        if($item){
            if($item->precio_bruto){
                $producto->precio = $item->precio_bruto;
            }else{
                $producto->precio = '';
            }
        }else{
            $producto->precio = '';
        }
        
        //Obtener productos relacionados
        $relacionados = Producto::
                        where('categoria_id', $producto->categoria_id)
                        ->where('estado',1)
                        ->where('id','!=', $producto->id)
                        ->take(10)
                        ->get(); 

        // Imagen relacionados
        foreach($relacionados as $rel){

            $img_rel = ImagenProducto::
            where('tipo', 'principal_frontal')
            ->where('producto_id', $rel->id)
            ->first();

            if($img_rel){
            //transformacion a link img producto principal                      
            $lastSegment = basename(parse_url($img_rel->img_thumb_url, PHP_URL_PATH));
            $rel->imagen_url = self::base_url.$lastSegment;
            }else{
            
            $rel->imagen_url = "";
            }

            $rel_cat = Categoria::where('id', $rel->categoria_id)
                                    ->first();

            $rel->cat = $rel_cat->nombre;
        }
       //Fin relacionados
    //Creacion de arreglo para guardar imagen principal + imagenes de carrusel
        $imagenes = [];
    /* Obtener imágenes producto */
    // Imagen de portada
        $portada = ImagenProducto::
                            where('tipo', 'portada_escritorio')
                            ->where('producto_id', $producto->id)
                            ->first(); 
        
        if($portada){
           //transformacion a link img portada
            $lastSegment = basename(parse_url($portada->imagen_url, PHP_URL_PATH));
            $portada->imagen_url = self::base_url.$lastSegment;
        }else{
            $portada = new ImagenProducto();
            $portada->imagen_url ="";
        }

    
    
    // Imagen de producto principal
        $producto_img = ImagenProducto::
                            where('tipo', 'principal_frontal')
                            ->where('producto_id', $producto->id)
                            ->first();

         if($producto_img){
               //transformacion a link img producto principal                      
            $lastSegment = basename(parse_url($producto_img->img_thumb_url, PHP_URL_PATH));
            $producto_img->imagen_url = self::base_url.$lastSegment;
        }else{
            $producto_img = new ImagenProducto();
            $producto_img->imagen_url = "";
        }
     
    //Insercion img principal al arreglo imagenes
        array_push($imagenes, $producto_img);
    //imagen de carrusel de producto
        $producto_carrusel = ImagenProducto::
                            where('tipo', 'carrusel')
                            ->where('producto_id', $producto->id)
                            ->get();
        
        if($producto_carrusel){
             //transformacion de link a imagenes carrusel       
            foreach ($producto_carrusel as $aux){
                $lastSegment = basename(parse_url($aux->img_thumb_url, PHP_URL_PATH));
                $aux->imagen_url = self::base_url.$lastSegment;
                array_push($imagenes, $aux);
            }
        }else{
            $producto_carrusel = new ImagenProducto();
            $producto_carrusel->imagen_url = "";
        }
   
        $producto_cat = Categoria::where('id', $producto->categoria_id)
                                    ->first();
        
        if (!$producto_cat) {
            return abort(404);
        }
        
        // Obtener informacion de productos

        $campos = CampoProducto::where('producto_id', $producto->id)
                    ->orderBy('clasificacion_id', 'asc')
                    ->get();

        $clasificacion = Clasificacion::where('estado', 1)->get();

    
        if(count($campos) > 0){
            foreach ($clasificacion as $cat){
                $cat->cont=0;
                foreach ($campos as $cam){
                    if($cam->clasificacion_id == $cat->id){
                        $cat->cont++;
                    }      
                }
            }
        }
        


        return view('producto-detalle', compact('producto', 'portada','producto_img', 'imagenes', 'producto_cat', 'clasificacion', 'campos', 'relacionados'));
        
    }
}
