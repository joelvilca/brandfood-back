<?php

namespace App\Http\Controllers\Orders;

use App\Order;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use App\Events\orders;
use App\Http\Resources\OrderResource;

class CreateProcessController extends Controller
{
    public function __invoke(OrderRequest $request)
    {
        $order = Order::make([
            'creation_date' => now()->format('Y-m-d'),
        ]);

        $order->save();
        $order->products()->sync($request->products);
        $data = new OrderResource($order);
        broadcast(new orders($data));
        return response()->json(null, 201);
    }
}
