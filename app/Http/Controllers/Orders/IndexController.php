<?php

namespace App\Http\Controllers\Orders;

use App\Order;
use App\Events\orders;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;

class IndexController extends Controller
{
    public function __invoke()
    {
        $orders = Order::all();
        return OrderResource::collection($orders);
    }
}
