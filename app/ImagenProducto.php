<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ImagenProducto extends Model
{
    const PORTADA = 'portada';
    const PRINCIPAL = 'principal';
    const SECUNDARIA = 'secundaria';

    const PORTADA_ESCRITORIO = 'portada_escritorio';
    const PORTADA_MOVIL = 'portada_movil';
    const PRINCIPAL_FRONTAL = 'principal_frontal';
    const PRINCIPAL_REVERSO = 'principal_reverso';
    const CARRUSEL = 'carrusel';

    const UBICACION_STORAGE = 'public/productos/imagenes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imagen_url',
        'tipo',
        'estado',
        'titulo',
        'descripcion',
        'img_thumb_url',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'estado' => 'boolean',
    ];

    public function producto(): BelongsTo
    {
        return $this->belongsTo('App\Producto');
    }
}
