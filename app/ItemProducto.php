<?php

namespace App;

use App\Helpers\Constant;
use App\Helpers\File;
use App\Helpers\Util;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class ItemProducto extends Model
{
    use Notifiable;

    const UN = 'UN';
    const SACO = 'SACO';
    const STORAGE_URL = 'public/excel';
    const REPORTE_STOCK = 'Reporte_Items_Stock';
    const REPORTE_SIN_STOCK = 'Reporte_Items_Sin_Stock';
    const REPORTE_ITEM = 'Reporte_Items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'formato_venta',
        'SKU',
        'color',
        'estado',
        'precio_bruto',
        'slug',
        'stock_minimo',
        'talla',
        'color_codigo',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'estado' => 'boolean',
    ];

    /**
     * Set the product's slug.
     *
     * @param string $value
     *
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::of($value)->slug('-');
    }

    public function producto(): BelongsTo
    {
        return $this->belongsTo('App\Producto');
    }

    public function precios(): HasMany
    {
        return $this->hasMany('App\Precio');
    }

    public function detalles(): HasMany
    {
        return $this->hasMany('App\DetalleMovimiento');
    }

    public function dimension(): HasOne
    {
        return $this->hasOne('App\Dimension');
    }

    public function imagen(): HasOne
    {
        return $this->hasOne('App\ImagenItem');
    }

    public function stocks(): HasMany
    {
        return $this->hasMany('App\BodegaItemProducto', 'item_producto_id');
    }

    public function tieneStocks()
    {
        return $this->stocks()
                    ->exists();
    }

    public function perteneceCafeTostado(): bool
    {
        $producto = $this->producto;

        return $producto->categoria->esCafeTostado();
    }

    public function actualizarStock($bodega, $cantidad)
    {
        $bodegaItemProducto = BodegaItemProducto::where('bodega_id', $bodega->id)
                                                ->where('item_producto_id', $this->id)
                                                ->first();

        if ($bodegaItemProducto) {
            $bodegaItemProducto->update([
                'stock' => $bodegaItemProducto->stock + $cantidad,
            ]);
        } else {
            $bodegaItemProducto = BodegaItemProducto::make([
                'stock' => $cantidad,
            ]);

            $bodegaItemProducto->bodega()->associate($bodega->id);
            $bodegaItemProducto->itemProducto()->associate($this);
            $bodegaItemProducto->save();
        }
    }

    public function obtenerStock($bodegaID)
    {
        $bodegaItemProducto = BodegaItemProducto::where('bodega_id', $bodegaID)
                                                ->where('item_producto_id', $this->id)
                                                ->first();

        if ($bodegaItemProducto) {
            return $bodegaItemProducto->stock;
        }

        return 0;
    }

    public function hayStockPositivo($bodega, $cantidad): bool
    {
        $bodegaItemProducto = BodegaItemProducto::where('bodega_id', $bodega->id)
                                                ->where('item_producto_id', $this->id)
                                                ->first();

        $stock = $bodegaItemProducto ? $bodegaItemProducto->stock : 0;

        if ($stock + $cantidad < 0) {
            return false;
        }

        return true;
    }

    public function verfificarStockExistente($bodega, $cantidad): bool
    {
        $bodegaItemProducto = BodegaItemProducto::where('bodega_id', $bodega->id)
                                                ->where('item_producto_id', $this->id)
                                                ->first();

        if ($bodegaItemProducto->stock - $cantidad < 0) {
            return false;
        }

        return true;
    }

    public function tieneMasItemsQueElStockMinimo($bodega): bool
    {
        $bodegaItemProducto = BodegaItemProducto::where('bodega_id', $bodega->id)
                                                ->where('item_producto_id', $this->id)
                                                ->first();

        if (!$bodegaItemProducto) {
            return true;
        }

        if ($bodegaItemProducto->stock < $this->stock_minimo) {
            return false;
        }

        return true;
    }

    public function quitarStock($bodega, $cantidad): bool
    {
        $bodegaItemProducto = BodegaItemProducto::where('bodega_id', $bodega->id)
                                                ->where('item_producto_id', $this->id)
                                                ->first();

        $bodegaItemProducto->update([
            'stock' => $bodegaItemProducto->stock - $cantidad,
        ]);

        return true;
    }

    public function agregarStock($bodega, $cantidad): bool
    {
        $bodegaItemProducto = BodegaItemProducto::where('bodega_id', $bodega->id)
                                                ->where('item_producto_id', $this->id)
                                                ->first();

        $bodegaItemProducto->update([
            'stock' => $bodegaItemProducto->stock + $cantidad,
        ]);

        return true;
    }

    public function calcularSubTotal(int $cantidad): int
    {
        return $this->precio_bruto * $cantidad;
    }

    public static function generarNombreReporteExcel($tipoReporte)
    {
        $fechaActual = Util::obtenerFechaActual('d-m-Y H_i_s');

        $nombre = $tipoReporte.'_'.$fechaActual.'.'.Constant::EXCEL_EXTENSION;

        return $nombre;
    }

    public static function generarUbicacionReporte($nombre)
    {
        $ubicacion = File::generarRutaRelativa(self::STORAGE_URL, $nombre);

        return $ubicacion;
    }
}
