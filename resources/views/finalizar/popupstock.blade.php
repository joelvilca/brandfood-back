<v-dialog
transition="dialog-top-transition"
v-model="dialogVisible"
scrollable
max-width="400"
:fullscreen="$vuetify.breakpoint.mobile"
>
<template v-slot:default="dialog">
    <v-card style="overflow-y: hidden;">
      <v-toolbar
        style="margin-bottom:15px; letter-spacing: 1.8px; font-size: 1.5rem; height:100px; padding:25px 20px; flex:unset;"
        color="grey lighten-3"
        text-align="center"
      ><v-spacer></v-spacer><h4 style="text-align:center">Productos sin stock </h4><v-spacer></v-spacer>
      <v-btn
      icon
      color="black"
      @click="dialog.value = false"
      style="position:absolute; right: 0px; top: -24px;"
    >
    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
      <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
  </svg>
    </v-btn>
    </v-toolbar>
      <v-card-text>
        <p style="word-break: normal; font-size: 0.75em">
            Lamentamos informar que no tenemos stock disponible de los siguientes productos:
          </p>
          <div class="order-details">
            <div
              class="inner-order col-md-12"
              style="height: 65px; margin-bottom: 5px"
              v-for="(shop, index) in errorITems"
              :key="index"
            >
              <div class="label-left">
                <p
                  style="
                    v-cloak
                    font-size: 0.85em !important;
                    font-weight: 600;
                    margin-bottom: 0;
                  "
                >
                  @{{ shop.nombre_producto }}
                  <br />
                  <span
                    style="
                      v-cloak
                      font-size: 10px !important;
                      font-weight: 600;
                      margin-bottom: 0;
                    "
                    >@{{ shop.nombre }}</span
                  >
                  <br />
                  <span
                    style="
                      v-cloak
                      font-size: 10px !important;
                      font-weight: 600;
                      margin-bottom: 0;
                    "
                    >Stock Disponible: @{{ shop.stock_disponible }}</span
                  >
                  <br />
                </p>
              </div>
            </div>
          </div>
      </v-card-text>
      <v-divider></v-divider>
      <v-card-actions class="justify-start">
        <v-row align="center">
            <v-col
              cols="12"
              sm="12"
              style="padding: 20px 16px 20px 16px !important;"
            >
              <div class="text-center">
                <a href="{{URL::to('/')}}">
                    <v-btn
                    text
                    small
                    
                    >Volver a la tienda</v-btn>
                </a>
              </div>
            </v-col>
        </v-row>
    </v-card-actions>
    </v-card>
  </template>
</v-dialog>