<!DOCTYPE html>
<html lang="es-ES" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5"/>
        <title>Tienda Online | Patagonia Blend | Una taza con identidad</title>
        <meta name="description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta name="keywords" content="cafe de especialidad, te en hebras, infusiones, cafeteras, cafe verde">
        <meta name="author" content="Patagonia Blend">
        <link rel="shortcut icon" href="https://patagoniablend.cl/patagonia_blend/img/master/favicon.png">
        <meta property="og:title" content="Tienda Online de Patagonia Blend">
        <meta property="og:description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta property="og:image" content="https://patagoniablend.cl/patagonia_blend/img/master/logo-movil.png">
        <meta property="og:url" content="https://tienda.patagoniablend.cl">
        <meta property="og:site_name" content="Tienda Online de Patagonia Blend" />
        <meta property="twitter:title" content="Tienda Online de Patagonia Blend">
        <meta property="twitter:description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta property="twitter:image:src" content="https://patagoniablend.cl/patagonia_blend/img/master/logo-movil.png">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/style.css">
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">        
        <link
        href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css"
        rel="stylesheet"
        />
        @include('base.facebook')
        @include('base.hubspot')
        @include('base.google')
    </head>
    <body>
    <div id="app">
      <v-app>
        @include('base.toolbar')
        <v-container style="min-height:250px;">
          <div class="main">
            <div class="section" style="padding:2rem 0 !important;" id="step">
              <div class="row">
                <div class="col" id="finalizar-card" style="min-height:500px;">
                  <v-stepper v-cloak v-model="e6" vertical>
                    <v-stepper-step :complete="e6 > 1" step="1">
                      Información de contacto
                    </v-stepper-step>
                    <v-stepper-content step="1">
                      <v-form ref="form" lazy-validation>
                        <v-card class="mb-5" id="card-sin-border">
                          <div class="">
                            <div class="" style="margin-top: 8px">
                              <div class="row">
                                <v-col
                                cols="12"
                                sm="12"
                                md="12"
                                class="ajustar-input"
                              >
                                    <v-text-field
                                      outlined
                                      dense
                                      id="email"
                                      label="Correo electrónico"
                                      placeholder="Correo electrónico"
                                      name="email"
                                      type="email"
                                      :rules="[
																				(v) => !!v || 'Introduce un correo electrónico válido',
																				(v) =>
																					/.+@.+\..+/.test(v) || 'Email no es valido.',
																			]"
                                      v-model="email"
                                      @change="searchForEmail"
                                    />
                                </v-col>
                                <v-col
                                  cols="12"
                                  sm="12"
                                  md="6"
                                  class="ajustar-input"
                                >
                                  <v-text-field
                                  id="nombre"
                                  label="Nombre"
                                  placeholder="Ingrese su nombre"
                                  outlined
                                  dense
                                  :rules="[
																				(v) => !!v || 'Introduce un nombre',
																				(v) =>
																					/^[a-zA-Z\s]/.test(v) ||
																					'No esta permitido numeros.',
																			]"
                                  name="nombre"
                                  type="text"
                                  v-model="form.nombre"
                                  ></v-text-field>
                                </v-col>
                                <v-col
                                  cols="12"
                                  sm="12"
                                  md="6"
                                  class="ajustar-input"
                                >
                                  <v-text-field
                                      outlined
                                      dense
                                      label="Apellido"
                                      id="apellidos"
                                      placeholder=" su apellido"
                                      :rules="[
																				(v) => !!v || 'Introduce un apellido',
																				(v) =>
																					/^[a-zA-Z\s]/.test(v) ||
																					'No esta permitido numeros.',
																			]"
                                      name="apellidos"
                                      type="text"
                                      v-model="form.apellidos"
                                  ></v-text-field>
                                </v-col>
                                <v-col
                                cols="12"
                                sm="12"
                                md="12"
                                class="ajustar-input"
                                >
                                    <v-text-field
                                    id="direccion"
                                    outlined
                                    dense
                                    label="Dirección"
                                    placeholder="Ingrese su dirección para despacho"
                                    :rules="[
                                      (v) => !!v || 'Introduce una dirección',
                                    ]"
                                    name="direccion"
                                    type="text"
                                    v-model="form.direccion"
                                     />
                                </v-col>
                                <v-col
                                  cols="12"
                                  sm="12"
                                  md="12"
                                  class="ajustar-input"
                                >
                                  <v-autocomplete
                                  v-model="form.ciudad"
                                  label="Ciudad"
                                  :items="ciudades"
                                  outlined
                                  dense
                                  :rules="[
                                    (v) => !!v || 'Introduce la ciudad',
                                  ]"
                                  item-value="id"
                                  item-text="nombre"
                                ></v-autocomplete>
                                </v-col>

                                <v-col
                                cols="12"
                                sm="12"
                                md="12"
                                class="ajustar-input"
                              >
                                  <v-text-field
                                  outlined
                                  dense
                                  id="telefono"
                                  label="Teléfono"
                                  placeholder="Introduce un número de teléfono válido"
                                  :rules="[
                                    (v) => !!v || 'Campo teléfono es requerido',
                                    (v) =>
                                      /^[0-9]/.test(v) ||
                                      'No esta permitido letras.',
                                    (v) =>
                                      form.telefono.length >= 9 ||
                                      'Teléfono inválido.',
                                  ]"
                                  name="telefono"
                                  type="text"
                                  v-model="form.telefono"
                                />
                              </v-col>
                              <v-col
                                  cols="12"
                                  sm="12"
                                  md="12"
                                  class="ajustar-input"
                              >
                                  <v-textarea
                                  label="Notas al pedido"
                                  outlined
                                  dense
                                  auto-grow
                                  id="notas"
                                  name="notas"
                                  v-model="form.notas"
                                ></v-textarea>
                              </v-col>

                              <v-col
                              cols="12"
                              sm="12"
                              md="12"
                              class="ajustar-input"
                              v-if="!isCupon"
                              >
                              <small>¿Tienes un código de descuento?</small>
                              <p>
                                <a
                                  @click="isCupon = true"
                                  style="color: #2196f3"
                                  >Ingrese su código</a
                                >
                              </p>
                              </v-col>
                              <v-col
                              cols="12"
                              sm="12"
                              md="12"
                              v-else
                              >
                                  <v-text-field
                                  label="Código de descuento"
                                  placeholder="Ingresa el código de descuento"
                                  outlined
                                  dense
                                  v-model="cupon"
                                  :error="!!errorCupon"
                                  :error-messages="errorCupon"
                                  :disabled="isCuponAply"
                                  @keyup="
                                    () => {
                                      errorCupon = ''
                                    }
                                  "
                                  
                                >
                                  <template v-slot:append>
                              
                                      <v-btn
                                      small
                                      dark
                                      color="primary"
                                      style="margin-top:-4px;"
                                      @click="validCupon"
                                      :disabled="isCuponAply"
                                      >
                                      
                                        APLICAR
                                        
                                      </v-btn>
                                  </template>
                                    
                                </v-text-field>
                                <v-btn
                                v-if="isCuponAply"
                                x-small
                                fab
                                flat
                                @click="deleteCupon"
                                style="
                                  overflow: hidden;
                                  position: absolute;
                                  margin-top: -63px;
                                  right:5px;
                                
                                "
                              >
                                <v-icon
                                  
                                  style="font-size: 2em !important"
                                >
                                  mdi-close
                                </v-icon>
                              </v-btn>
                              <span v-if="isCuponAply" class="message" style="font-size: 0.65em !important; margin-left:12px;">Cupón aplicado exitosamente</span>

                              </v-col>
                            </div>

                           
                              
                            </div>
                          </div>
                          <br>
                          <v-btn
                            color="primary"
                            large
                            @click="submitCotizacion"
                            :loading="loading"
                            :disabled="loading"
                            >Continuar</v-btn
                          >
                        </v-card>
                      </v-form>
                    </v-stepper-content>

                    <v-stepper-step :complete="e6 > 2" step="2"
                      >Forma de Entrega
                    </v-stepper-step>
                    <v-stepper-content step="2">
                      <p style="font-size: 0.8em; text-align: justify">
                        Seleccione la forma de entrega. Recuerda que despachamos
                        el
                        <strong>día hábil siguiente</strong>.
                      </p>
                      <v-card
                        class="mb-5"
                        style="margin-top: 8px"
                        id="card-sin-border"
                      >
                        <v-list v-if="courier.length > 0">
                        <v-radio-group v-model="courier_id">
                              <v-radio v-for="(item, index) in courier" :key="index" :value="index" @change="addCourier(item)" class="listado-courier">
                                <template v-slot:label>
                                  <div style="display: table-cell;"> @{{ item.nombre_courier }}</div>
                                  <div v-cloak style="display: table-cell; text-align: right; padding-left: 0.75em;">  $@{{
                              formatMoney(item.costo) }}</div>
                                </template>
                              </v-radio>
                            </v-radio-group>
                        </v-list>
                      </v-card>
                      <v-btn v-if="courier_item" color="primary" large @click="e6 = 3"
                        >Continuar</v-btn
                      >
                      <v-btn v-else disabled large>Continuar</v-btn>
                      <v-btn plain @click="_onBack">Volver</v-btn>
                    </v-stepper-content>
                    <v-stepper-step :complete="e6 > 3" step="3"
                      >Forma de Pago</v-stepper-step
                    >
                    <v-stepper-content step="3">
                      <v-card id="card-sin-border">
                        <p style="font-size: 0.8em; text-align: justify">
                          Seleccione la forma de pago:
                        </p>
                        <div class="col-md-12" style="padding:0 !important;">
                          <v-list>
                          <v-radio-group v-model="pago">
                              <v-radio v-for="(item, index) in pagos" :key="index" :value="item" class="listado-courier">
                                <template v-slot:label>
                                  <div>@{{ item }}</div>
                                </template>
                              </v-radio>
                            </v-radio-group>
                          </v-list>
                        </div>
                        <p style="font-size: 1em; text-align: justify">
                          <strong>
                            Total a pagar: <span v-cloak style="font-weight:700;">$@{{ formatMoney( getPrecioTotal() +
                            parseFloat(courier_item.costo) - descuento ) }} </span>
                          </strong>
                        </p>
                      </v-card>
                      <v-btn
                        v-if="pago"
                        style="clear: both"
                        color="primary"
                        large
                        @click="pagar"
                        :loading="loading2"
                        :disabled="loading2"
                        >Pagar</v-btn
                      >
                      <v-btn v-else disabled large>Pagar</v-btn>
                      <v-btn @click="e6 = 2" plain>Volver</v-btn>
                    </v-stepper-content>
                  </v-stepper>
                </div>
                <div class="col-md-5" id="finalizar-card">
                  <div class="order-summary" style="background-color: #f2f2f2">
                    <h5>Resumen del Pedido</h5>

                    <div class="order-details">
                      <hr class="summary-line" />
                      <div
                        v-cloak
                        class="inner-order col-md-12"
                        style="height: 50px"
                        v-for="(shop, index) in shopping"
                        :key="index"
                      >
                        <div class="label-left">
                          <span class="finalizar-badge">
                            @{{ shop.cantidad }}
                          </span>
                          <img :src="shop.imagen" width="25" height="25" />
                          <p
                            style="
                              font-size: 0.85em !important;
                              font-weight: 600;
                              margin-bottom: 0;
                              line-height: 13px;
                            "
                          >
                            @{{ shop.producto }}
                            <br />
                            <span
                              style="
                                font-size: 10px !important;
                                font-weight: 500;
                                margin-top: -5px;
                              "
                              >@{{ shop.nombre }}</span
                            >
                          </p>
                        </div>
                        <div class="product-total">
                          <p v-cloak style="font-weight: bold">$ @{{ formatMoney(shop.total) }}</p>
                        </div>
                      </div>
                    </div>
                    <hr class="summary-line" />
                    <div class="col-md-12" style="padding:0 0 !important;">
                      <table style="margin-bottom: 10px;">
                        <tr>
                          <td style="width: 100px">Subtotal:</td>
                          <td v-cloak style="width: 80px; text-align:right; font-weight: bold;">
                            $@{{ formatMoney(getPrecioTotal()) }}
                          </td>
                        </tr>
                        <tr v-if="descuento != 0">
                          <td style="width: 100px">Descuento cupón:</td>
                          <td v-cloak style="width: 80px; text-align:right; font-weight: bold;">
                            $@{{ formatMoney(parseFloat(descuento)) }}
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 100px">Despacho:</td>
                          <td v-cloak style="width: 80px; text-align:right;font-weight: bold;" v-if="courier_item != ''">
                            $@{{ formatMoney(parseFloat(courier_item.costo)) }}
                          </td>
                          <td style="width: 80px; text-align:right; font-size:0.55em;" v-else>
                            Calculado en el siguiente paso
                          </td>
                        </tr>
                        <tr v-if="descuento != 0 && courier_item === ''">
                          <td style="width: 100px; font-weight: 700">Total:</td>
                          <td v-cloak style="width: 80px; font-weight: 700; text-align:right;font-weight: bold;">
                            $@{{ formatMoney(getPrecioTotal() - descuento) }}
                          </td>
                        </tr>
                        <tr v-if="courier_item != '' && courier_item != 0">
                          <td style="width: 100px; font-weight: 700">Total:</td>
                          <td v-cloak style="width: 80px; font-weight: 700; text-align:right;"  v-if="courier_item != '' && courier_item != 0">
                            $@{{ formatMoney( getPrecioTotal() +
                            parseFloat(courier_item.costo) - descuento ) }}
                          </td>
                        </tr>
                        <tr v-if="descuento == 0 && courier_item === ''">
                          <td style="width: 100px; font-weight: 700">Total:</td>
                          <td v-cloak style="width: 80px; font-weight: 700; text-align:right;">$@{{ formatMoney(getPrecioTotal()) }}</td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @include('finalizar.popupstock')
          </div>
        </v-container>
      </v-app>
    </div>
		<script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
    
    <script src="{{URL::to('/')}}/js/axios.min.js"></script>
    <script src="{{URL::to('/')}}/js/finalizar.js"></script>

    <style type="text/css">

      @media (max-width: 425px) {
        #finalizar-card{
          padding:0 !important;
          border-radius:0 !important;
        }
      }
      .ajustar-input {
        padding-bottom:0px !important; 
      }
      .message {
        margin-top:-25px;
        position:absolute;
        color: green;
      }

      .v-stepper--vertical .v-stepper__content {
          padding: 16px 50px 16px 13px;
          width: auto;
      }
      .v-input__slot{
        padding-top: 2px!important;
        border-radius: 5px;
      }

      .v-application--is-ltr .v-stepper--vertical .v-stepper__content{
        margin: -8px -32px -16px 2rem !important;
      }

      .v-input--selection-controls{
        margin-top: unset;
        padding-top: unset;
      }
            
      .finalizar-badge{
              position: absolute;
              width: 18px;
              height: 18px;
              margin-top: -18px;
              text-align: center;
              margin-left: 18px;
              border-radius: 25px;
              -webkit-border-radius: 25px;
              font-size: 12px;
              background: rgba(114,114,114,0.9);
              color: #fefefe;
              padding: 2px 5px 1px 5px;
              font-weight: 700;
              line-height: normal;
              z-index: 9;
              
      }



      .listado-courier{
        border-radius: 5px;
        border:1px solid #cccccc;
        padding: 1.5rem 3%;
        font-size:14px;
        display: table-cell;
      }

      /* .listado-courier.v-input__slot{
        padding: unset !important;
      } */
      .message {
        margin-top:-25px;
        position:absolute;
        color: green;
      }
      .v-input--radio-group--column .v-radio{
        border-radius: 5px;
        margin-bottom: 0px;
      }
      .v-input--radio-group--column .v-radio:not(:last-child):not(:only-child) {
      margin-bottom: 0px;
      border-radius: 5px 5px 0 0;
      }
      .v-input--radio-group--column .v-radio:last-child {
      margin-bottom: 0px;
      border-radius: 0px 0px 5px 5px;
      }
    </style>
  </body>
</html>
