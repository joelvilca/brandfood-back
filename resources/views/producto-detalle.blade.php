<!DOCTYPE html>
<html lang="es-ES" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5"/>
        <title>{{$producto->nombre}} | Tienda Online | Patagonia Blend</title>
        <meta name="description" content="{{$producto->descripcion_larga}}">
        <meta name="keywords" content="cafe de especialidad, te en hebras, infusiones, cafeteras, cafe verde, cafe en grano">
        <meta name="author" content="Patagonia Blend">
        <link rel="shortcut icon" href="https://patagoniablend.cl/patagonia_blend/img/master/favicon.png">
        <link rel="canonical" href="{{URL::to("/")}}/producto/{{ $producto->slug }}"/>
        <link rel="stylesheet" href="{{URL::to('/')}}/css/style.css">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/buttonNumber.css">
        <link
        href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css"
        rel="stylesheet"
        />
        @include('base.facebook')
        @include('base.hubspot')
        @include('base.google')
        @include('producto.redes_sociales')
        @include('producto.data_estructurada')
        @include('base.fbViewContent')
    </head>
    <body>
		<div id="app">
    <v-app>
    @include('base.toolbar')
    @include('base.carro')
    <v-snackbar
      v-model="snackbar"
      :timeout="timeout"
      :color="color"
      max-width="300"
      outlined
      top
      multi-line
    >
      @{{texto}}
    </v-snackbar>
      <div class="pages-header-tienda">
        <div class="banner-producto">
          <img
            loading="lazy"
            style="width:100%;"
            src="{{ $portada->imagen_url }}"
          />
        </div>
      </div>
      <div class="main" style="background-color: white;margin:0px !important; ">
        <div class="section" style="padding:10px;">
          <div class="product-container">
            <div class="row">
              <div class="col-md-5" style="padding:0 !important;">
                <div class="product-photo-gallery">
                  <div class="product-leftside">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false" style="position: relative;">
                        <div class="carousel-inner">
                          <div class="item active srle">
                            <img loading="lazy" src="{{$producto_img->imagen_url}}" alt="" class="img-responsive">
                          </div>
                        </div>

                        {{-- <!-- Thumbnails --> 
                        <ul class="thumbnails-carousel" style="margin-top: 5px; padding-left:0 !important;">
                          @foreach ($imagenes as $img)
                              <li><img loading="lazy" src="{{$img->imagen_url}}" alt=""></li>
                          @endforeach      
                      </ul> --}}
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-md">
                <div class="product-description" style="float: left">
                <p style="color: #b24835; margin-bottom:8px; font-size:11px;">
                    
                    {{ $producto_cat->nombre }}
                    
                    
                </p>

                  <h4 style="margin-bottom: 15px;">{{ $producto->nombre }}</h4>

                  <p style="text-align: justify">
                    {{ $producto->descripcion_larga }}
                  </p>

                  <div class="order-box">
                    {{-- ITEMS POR PRODUCTO --}}
                    @include('producto.listado_items')
                  </div>
                  <div class="order-box">
                    <div
                      class="btn-order"
                      id="boton-espacio"
                    >
                      <button
                        class="btn"
                        id="ver-producto-btn2"
                        style="width:100%; font-weight:600; letter-spacing:2px;"
                  @click="addCart('{{$producto->id}}',{{$producto->precio}}, '{{$producto->nombre}}')"
                      >
                        Agregar al
                        Carro
                      </button>
                    </div>
                  </div>
                  <div class="order-box">
                    <p
                      style="
                        text-align: left;
                        font-size: 0.68em;
                        margin-left: 15px;
                        margin-top: 15px;
                      "
                    >
                      <small>* Venta disponible sólo para Chile</small>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="tabs-texto">
        <div>
          <v-expansion-panels v-model="tab" focusable>
            @foreach($clasificacion as $cat)
              @if($cat->cont > 0)
                <v-expansion-panel       
                  :key="{{$cat->id}}"              
                >
                  <v-expansion-panel-header style="font-weight: 700; letter-spacing: 2.5px !important; display: unset !important;">
                    <svg style="width:12px;height:12px; " viewBox="0 0 24 24">
                      <path fill="currentColor" d="M2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12M10,17L15,12L10,7V17Z" />
                  </svg>
                    {{$cat->nombre}}
                   
                  </v-expansion-panel-header>
                  <v-expansion-panel-content>
                    <v-simple-table dense>
                      <template v-slot:default>
                        <tbody>
                          @foreach($campos as $camp)
                            @if($camp->clasificacion_id == $cat->id)
                            <tr>
                                <td style="width: 120px">{{ $camp->nombre_campo }}</td>
                                <td>{{ $camp->valor_campo }}</td>
                              </tr>
                          
                            @endif
                              
                                
                          @endforeach
                        </tbody>
                      </template>
                    </v-simple-table>
                  </v-expansion-panel-content>
                </v-expansion-panel>
              @endif
            @endforeach
          </v-expansion-panels>
        </div>
      </div>
        
        <div class="main">
          <div class="section">
            <div class="section-title">
              <h2>Productos relacionados</h2>
              <hr class="center" />
            </div>
            <div class="scrolling-wrapper">
              @include('producto.relacionado')
                  
      
            </div>
          </div>
        </div>
      </div>
      <!-- MAIN FOOTER -->
      @include('base.footer')
        </v-app>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
        <script src="{{URL::to('/')}}/js/axios.min.js"></script>
        <script src="{{URL::to('/')}}/js/producto.js"></script>
    </body>
</html>