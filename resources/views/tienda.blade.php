<!DOCTYPE html>
<html lang="es-ES" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5"/>
        <title>Tienda Online | Patagonia Blend | Una taza con identidad</title>
        <meta name="description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta name="keywords" content="cafe de especialidad, te en hebras, infusiones, cafeteras, cafe verde">
        <meta name="author" content="Patagonia Blend">
        <link rel="shortcut icon" href="https://patagoniablend.cl/patagonia_blend/img/master/favicon.png">
        <meta property="og:title" content="Tienda Online de Patagonia Blend">
        <meta property="og:description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta property="og:image" content="https://patagoniablend.cl/patagonia_blend/img/master/logo-movil.png">
        <meta property="og:url" content="https://tienda.patagoniablend.cl">
        <meta property="og:site_name" content="Tienda Online de Patagonia Blend" />
        <meta property="twitter:title" content="Tienda Online de Patagonia Blend">
        <meta property="twitter:description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta property="twitter:image:src" content="https://patagoniablend.cl/patagonia_blend/img/master/logo-movil.png">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/style.css">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/buttonNumber.css">
      <link
        href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css"
        rel="stylesheet"
      />
      @include('base.facebook')
      @include('base.hubspot')
      @include('base.google')
    </head>
    <body>
		<div id="app">
    <v-app>
       @include('base.toolbar')
       @include('base.carro')
        <v-snackbar
        v-model="snackbar"
        :timeout="timeout"
        :color="color"
        outlined
        bottom
        top
      >
        @{{texto}}
      </v-snackbar>
       <img src="https://patagoniablend.cl/patagonia_blend/img/images/1018x152_2-Cafecito.svg" alt="Tienda Patagonia Blend" style="width:100%;margin-top:4.5rem;" id="banner">
      <div class="wrapper">
        <div class="main color-background" id="categorias" style="padding:0.1rem 0px 40px 0px !important; margin:0px 0px 0px 0px !important;min-height: 55rem !important">
          <div class="section">
            <div class="row">
              <div class="col-md-3 desktop-view">
                {{-- Listado de categorias con filtro --}}
                <div class="shop-left-sidebar" style="border-radius: 8px" id="category">
                  @include('tienda.categorias_desk')
                </div>
              </div>
              <div class="col-md-9" style="padding-bottom:75px;">
                <div class="shop-left-sidebar" style="border-radius: 25px; padding:0;">
                  <div class="search-box" style="border-radius: 25px; padding:0;">
                    <div class="search-container">
                      <div class="form-group" style="display: inline; border-radius: 25px; padding:0;">
                        <div class="input-group">
                          <input
                            id="buscador"
                            aria-label="Buscador" 
                            aria-describedby="Buscador de productos"
                            type="text"
                            placeholder="¿Qué estás buscando?"
                            class="form-control"
                            aria-describedby="basic-addon1"
                            v-model="search"
                            @click="onBusqueda()"
                            style="z-index:1;border-radius: 25px; outline:none;"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="responsive-view" style="margin-top: 15px; border-radius:8px;">
                  <div
                    style="
                      visibility: hidden;
                      position: absolute;
                      width: 0px;
                      height: 0px;
                    "
                  >
                    <svg xmlns="http://www.w3.org/2000/svg">
                      <symbol viewBox="0 0 20 20" id="expand-more">
                        <path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" />
                        <path d="M0 0h24v24H0z" fill="none" />
                      </symbol>
                      <symbol viewBox="0 0 20 20" id="close">
                        <path
                          d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
                        />
                        <path d="M0 0h24v24H0z" fill="none" />
                      </symbol>
                    </svg>
                  </div>
                  <div style="width: 100% !important" @click="openCatMovil()">
                    <details >
                      <summary
                        style="
                          font-weight: 500;
                          font-size: 16px;
                          font-family: 'Open Sans';
                          outline:none;
                        "
                      >
                        Categorías
                      </summary>
                    </details>
                  </div>
                </div>
                <div v-if="!loading" class="row" id="listado" style="margin:unset;">
                  @include('tienda.listado_productos')
                </div>
                <div v-else class="text-center" style="">
                  <v-progress-circular
                    indeterminate
                    style="color: #b24835; margin-top: 25px"
                  ></v-progress-circular>
                </div>
                <div
                  v-if="search != '' && datos.length === 0 && !loading1"
                  class="text-center"
                  style=""
                >
                  <h4 style="margin-top: 40px">Búsqueda sin resultados</h4>
                </div>
                <div v-if="loading1" class="text-center" style="">
                  <v-progress-circular
                    indeterminate
                    style="color: #b24835"
                  ></v-progress-circular>
                </div>
                <div
                  class="text-center"
                  style="
                    display: flex;
                    align-text: center;
                    justify-content: center;
                  "
                >
                  <button
                    type="button"
                    class="btn btn-lg btn-block"
                    style="width: 250px; margin-top: 35px"
                    id="ver-producto-btn2"
                    @click="getFilter('Todos')"
                  >
                    Ver todos
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- <!-- MAIN FOOTER --> --}}
      @include('base.footer')
      @include('tienda.compradirecta')
      @include('tienda.categorias_movil')
      </v-app>
    </div>
    <style>
   
    .item__list{
      background-color:white !important;
    }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>    
    <script src="{{URL::to('/')}}/js/axios.min.js"></script>
    <script src="{{URL::to('/')}}/js/tienda.js"></script>
    </body>
</html>