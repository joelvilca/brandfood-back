<v-dialog
transition="dialog-top-transition"
v-model="searchDialog"
scrollable
max-width="600"
:fullscreen="$vuetify.breakpoint.mobile"
>
<template v-slot:default="dialog">
  <v-card>
    <v-toolbar
      style="margin-bottom:15px;  height:60px; padding:0px 5px; flex:unset;"
      color="grey lighten-2"
      text-align="center"
      outlined
    ><v-spacer></v-spacer><h4 style="text-align:center; font-weight:500 !important;letter-spacing: 1.8px; font-size: 1.5rem;">Buscar</h4><v-spacer></v-spacer>
    <v-btn
    icon
    color="black"
    @click="dialog.value = false"
    style="position:absolute; right: 8px; top: 0px;"
  >
  <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
</svg>
  </v-btn>
  </v-toolbar>
    <v-card-text style="padding:0 8px !important;">
      <v-toolbar color="white" style="padding:0;">
        <v-autocomplete
          v-model="buscarProducto"
          :items="productosTotal"
          item-text="nombre"
          item-value="slug"
          hide-no-data
          hide-details
          label="Buscar en nuestra tienda"

          filled
          flat
          solo
          cache-items
          full-width
          single-line
          height="48"
        >
            <template v-slot:selection="data">
                <v-avatar>
                  <img :src="data.item.imagen_thumbnail_path" style="">
                </v-avatar>
                <p style="font-size:0.7em;margin-bottom:0; margin-left:7px;">
                  @{{ data.item.nombre }}
                </p>      
            </template>
            <template v-slot:item="data">
              <template>
                <v-list-tile-avatar>
                  <img :src="data.item.imagen_thumbnail_path" style="height:30px;">
                </v-list-tile-avatar>
                <v-list-tile-content>
                  <v-list-tile-title v-html="data.item.nombre" style="margin-left:8px; font-size: 0.70em;"></v-list-tile-title>
                </v-list-tile-content>
              </template>
            </template>
        </v-autocomplete>
        <v-btn icon @click="searchProduct()" :disabled="buscarProducto == ''">
          <svg style="width:26px;height:26px;color:#222;" viewBox="0 0 24 24">
            <path fill="currentColor" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
        </svg>
        </v-btn>
      </v-toolbar>
      <v-list style="margin-top:15px;">
          
          <v-list-item>
            <v-list-item-content>
              <v-list-item-title style="font-weight: 700">MENÚ PRINCIPAL</v-list-item-title>
              
            </v-list-item-content>
          </v-list-item>
          <v-divider></v-divider>
          <v-list-item>
            <a href="https://www.patagoniablend.cl" style="text-decoration: none; cursor:pointer; color:rgba(0,0,0,.87)!important;">
              <v-list-item-content>
                <v-list-item-title>Inicio</v-list-item-title>
              </v-list-item-content>
            </a>
          </v-list-item>
          <v-list-item>
            <a href="https://www.patagoniablend.cl/el-tesoro/" style="text-decoration: none; cursor:pointer; color:rgba(0,0,0,.87)!important;">
              <v-list-item-content>
                <v-list-item-title>El tesoro</v-list-item-title>
              </v-list-item-content>
            </a>
          </v-list-item>
          <v-list-item>
            <a href="https://www.patagoniablend.cl/nuestra-tostaduria/" style="text-decoration: none; cursor:pointer; color:rgba(0,0,0,.87)!important;">
              <v-list-item-content>
                <v-list-item-title>Nuestra tostaduría</v-list-item-title>
              </v-list-item-content>
            </a>
          </v-list-item>
          <v-list-item>
            <a href="https://www.patagoniablend.cl/trazabilidad-y-analisis/" style="text-decoration: none; cursor:pointer; color:rgba(0,0,0,.87)!important;">
              <v-list-item-content>
                <v-list-item-title>Trazabilidad y análisis</v-list-item-title>
              </v-list-item-content>
            </a>
          </v-list-item>
          <v-list-item>
            <a href="https://www.patagoniablend.cl/academia-del-cafe/" style="text-decoration: none; cursor:pointer; color:rgba(0,0,0,.87)!important;">
              <v-list-item-content>
                <v-list-item-title>Academia del café</v-list-item-title>
              </v-list-item-content>
            </a>
          </v-list-item>
          <v-list-item>
            <a href="https://www.patagoniablend.cl/estate-coffee/" style="text-decoration: none; cursor:pointer; color:rgba(0,0,0,.87)!important;">
              <v-list-item-content>
                <v-list-item-title>Estate coffee</v-list-item-title>
              </v-list-item-content>
            </a>
          </v-list-item>
          <v-list-item>
            <a href="https://www.patagoniablend.cl/microlotes/" style="text-decoration: none; cursor:pointer; color:rgba(0,0,0,.87)!important;">
              <v-list-item-content>
                <v-list-item-title>Microlotes</v-list-item-title>
              </v-list-item-content>
            </a>
          </v-list-item>
        </v-list>
        
    </v-card-text>
    <v-divider></v-divider>
    <v-card-actions class="justify-start">
      <v-row align="center">
          <v-col
            cols="12"
            sm="12"
            style="padding: 20px 16px 20px 16px !important;"
          >
            <div class="text-center">
              <v-btn
              text
              small
               @click="dialog.value = false"
              >Cerrar</v-btn>
            </div>
          </v-col>
      </v-row>
  </v-card-actions>
  </v-card>
</template>
</v-dialog>
