<v-dialog
transition="dialog-top-transition"
v-model="searchDialog"
scrollable
top
max-width="400"
>
<template v-slot:default="dialog">
  <v-card style="overflow-y: hidden;">
    <v-toolbar
    dark
    color="teal"
  >
    <v-autocomplete
      v-model="buscarProducto"
      :items="productosTotal"
      item-text="nombre"
      item-value="slug"
      hide-no-data
      hide-details
      label="Buscar productos"
      dense
      filled
      solo-inverted
    >
        <template v-slot:selection="data">
            <v-avatar>
              <img :src="data.item.imagen_thumbnail_path" style="">
            </v-avatar>
            <p style="width:100%; font-size:14px;margin-bottom:0;">
              @{{ data.item.nombre }}
            </p>      
        </template>
        <template v-slot:item="data">
          <template>
            <v-list-tile-avatar>
              <img :src="data.item.imagen_thumbnail_path" style="height:30px;">
            </v-list-tile-avatar>
            <v-list-tile-content>
              <v-list-tile-title v-html="data.item.nombre" style="margin-left:8px; font-size: 0.85em;"></v-list-tile-title>
            </v-list-tile-content>
          </template>
        </template>
    </v-autocomplete>
    <v-btn icon @click="searchProduct()" :disabled="buscarProducto == ''">
      <svg style="width:26px;height:26px;color:white;" viewBox="0 0 24 24">
        <path fill="currentColor" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
    </svg>
    </v-btn>
  </v-toolbar>
    {{-- <v-card-text style="padding:0" class="align-self-center">
		<br>
		<v-flex xs11>
            <v-autocomplete
              v-model="buscarProducto"
              :items="productosTotal"
              label="Buscar producto"
              item-text="nombre"
              item-value="slug"
							clearable
              filled
              
							style="margin:10px;"
            >
              <template v-slot:selection="data">
                  <v-avatar>
                    <img :src="data.item.imagen_thumbnail_path" style="width:30px; ">
                  </v-avatar>
                  @{{ data.item.nombre }}
              </template>
              <template v-slot:item="data">
                <template>
                  <v-list-tile-avatar>
                    <img :src="data.item.imagen_thumbnail_path" style="width:30px;">
                  </v-list-tile-avatar>
                  <v-list-tile-content>
                    <v-list-tile-title v-html="data.item.nombre"></v-list-tile-title>
                  </v-list-tile-content>
                </template>
              </template>
            </v-autocomplete>
          </v-flex>
					<br>
					<div style="display:flex;justify-content:center; padding:30px;">
					<v-btn block color="success" @click="searchProduct()">
        		buscar
      		</v-btn>
					</div>
    </v-card-text> --}}
  </v-card>
</template>
</v-dialog>