<v-dialog
transition="dialog-top-transition"
v-model="drawer"
scrollable
max-width="400"
:fullscreen="$vuetify.breakpoint.mobile"
>
<template v-slot:default="dialog">
  <v-card style="overflow-y: hidden;">
    <v-toolbar
      style="margin-bottom:15px; letter-spacing: 1.8px; font-size: 1.5rem; flex:unset;"
      color="grey lighten-2"
      text-align="center"
    ><v-spacer></v-spacer>Carrito<v-spacer></v-spacer>
    <v-btn
    icon
    color="black"
    @click="dialog.value = false"
    style="position:absolute; right:15px;"
  >
  <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
</svg>
  </v-btn></v-toolbar>
    <v-card-text style="padding:0" class="align-self-center">
        <v-list-item
            id="lista-items"
            v-for="(item, index) in shopping"
            :key="index"
            style="height:110px;"
      >
              <v-row
              no-gutters
              class="justify-center"
            >
              <v-col
                cols="3"
                class="flex-grow-1 flex-shrink-0"
              >
              <v-list-item-avatar style="margin-top:15px; width:3.5rem; height:auto; border-radius: unset !important;">
                <v-img :src="item.imagen" style="width:100%;"></v-img>
              </v-list-item-avatar>
              </v-col>
              <v-col
                cols="7"
                style="min-width: 100px; max-width: 100%;"
                class="flex-grow-0 flex-shrink-0"
              >
              <v-list-item-content>
                <v-list-item-title v-html="item.producto" style="margin-bottom:5px; font-size:1rem;"></v-list-item-title>
                <v-list-item-subtitle v-html="item.nombre" style="margin-bottom:5px;font-size:0.7rem;"></v-list-item-subtitle>
                <v-list-item-subtitle>
                  <div
                  class="product__quantity">
                  <button
                    :disabled="item.cantidad === 1"
                    @click="addCountDrawer(item, 'less')"
                    class="product__quantityBtn  less">
                    - 
                  </button>
                    <input
                      v-model="item.cantidad"
                      style="width: 45px;"
                      class="product__quantityText">
                    <button
                      :disabled="item.cantidad===item.stock_disponible"
                      @click="addCountDrawer(item, 'plus')"
                      class="product__quantityBtn plus">
                    + 
                    </button>
                </div>
                </v-list-item-subtitle>

               
              </v-list-item-content>
              </v-col>
              <v-col
                cols="1"
                class="flex-grow-0 flex-shrink-1"
              >
              <v-list-item-content>
                  <v-list-tile-action>
                    
                    <v-btn icon @click="removeShop(index)" style="margin-top:25px; position:absolute;">
                      <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
                    </svg>
                    </v-btn>
                  </v-list-tile-action>
              </v-list-item-content>
              </v-col>
            </v-row>
      </v-list-item>
      
    </v-card-text>
    <v-divider></v-divider>
    <v-card-actions class="justify-end">
        <v-row align="center">
            <v-col
              cols="12"
              sm="12"
              style="padding: 10px 16px 10px 16px !important;"
            >
            <v-list-item-title
              v-if="shopping.length"
              class="total-titulo text-center"
              
            >
              Total : $@{{ formatMoney(getPrecioTotal()) }}
            </v-list-item-title>
              <div class="text-center">
              <a v-bind:href="'{{URL::to('/')}}/finalizar-compra'">
                <v-btn
                    v-if="shopping.length > 0"
                    color="info"
                    block
                    dark
                    x-large
                    @click="checkout()"
                >Finalizar Compra</v-btn>
              </a>
                <v-btn
                text
                small
                 @click="dialog.value = false"
                >Seguir Comprando</v-btn>
              </div>
            </v-col>
        </v-row>
    </v-card-actions>
  </v-card>
</template>
</v-dialog>
