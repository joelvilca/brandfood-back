<div class="footer">
    <!-- MAIN FOOTER -->
    <div class="main">
      <div class="section">
        <div class="row">
          <div class="col-md-6 left-column">
            <div
              class="footer-logo"
              style="
                width: 100%;
                overflow: hidden;
                text-align: center;
                margin-bottom: 8px;
              "
            >
              <img
                loading="lazy"
                src="https://www.patagoniablend.cl/patagonia_blend/img/master/logo.png"
                alt="Patagonia Blend"
                title="Patagonia Blend"
              />
            </div>
            <div
              class="about-footer"
              style="
                padding: 0 7%;
                text-align: justify;
                font-size: 0.65em !important;
              "
            >
              <p>
                Patagonia Blend es una empresa regional con casa matriz en
                Punta Arenas, dedicada a la Tostaduría del café de
                especialidad y la elaboración de té en hebras con frutos
                deshidratados de la Patagonia.
              </p>
            </div>

            <div class="social-footer" style="text-align: center">
              <div class="social-items" style="margin-right: 20px">
                <a href="#">
                  <img
                    loading="lazy"
                    src="https://www.patagoniablend.cl/patagonia_blend/img/images/credenciales/credenciales-07.png"
                    height="65"
                    title="Credenciales Café de Colombia"
                    alt="Credenciales Café de Colombia"
                /></a>
              </div>
              <div class="social-items" style="margin-right: 20px">
                <a href="#">
                  <img
                    loading="lazy"
                    src="https://www.patagoniablend.cl/patagonia_blend/img/images/credenciales/credenciales-08.png"
                    height="65"
                    title="Marca Chile"
                    alt="Marca Chile"
                /></a>
              </div>

              <div class="social-items" style="margin-right: 20px">
                <a href="#">
                  <img
                    loading="lazy"
                    src="https://www.patagoniablend.cl/patagonia_blend/img/images/credenciales/credenciales-10.png"
                    height="65"
                    title="Rainforest Alliance"
                    alt="Rainforest Alliance"
                /></a>
              </div>
              <div class="social-items" style="margin-right: 20px">
                <a href="#">
                  <img
                    loading="lazy"
                    src="https://www.patagoniablend.cl/patagonia_blend/img/images/credenciales/credenciales-11.png"
                    height="65"
                    title="UTZ Certified"
                    alt="UTZ Certified"
                /></a>
              </div>
              <div class="social-items" style="margin-right: 20px">
                <a href="#">
                  <img
                    loading="lazy"
                    src="https://www.patagoniablend.cl/patagonia_blend/img/images/credenciales/credenciales-12.png"
                    height="65"
                    title="AMA Torres del Paine"
                    alt="AMA Torres del Paine"
                /></a>
              </div>
            </div>
          </div>
          <div class="col-md-3 right-column">
            <div class="footer-categories">
              <h5
                style="
                  color: white;
                  font-weight: 700;
                  letter-spacing: 1.5px;
                  margin-top: 32px;
                "
              >
                Informaciones
              </h5>
              <div class="categories-list" style="text-align: left">
                <div class="categoties-items" style="margin-bottom: 10px">
                  <a href="https://www.patagoniablend.cl/despachos-y-cobertura/"
                    >Despachos y Coberturas</a
                  >
                </div>
                <div class="categoties-items" style="margin-bottom: 10px">
                  <a href="https://www.patagoniablend.cl/cambios-y-devoluciones/"
                    >Cambios y devoluciones</a
                  >
                </div>
                <div class="categoties-items" style="margin-bottom: 10px">
                  <a href="https://www.patagoniablend.cl/covid-19"
                    >COVID-19</a
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 right-column">
            <div class="social-footer">
              <h5
                style="color: white; font-weight: 700; letter-spacing: 1.5px"
              >
                Síguenos
              </h5>
              <div class="social-items">
                <a
                  href="https://www.facebook.com/patagoniablend/"
                  target="_blank"
                  rel="noopener"
                  aria-label="Facebook"
                  ><div class="icon-fa">
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M12 2.04C6.5 2.04 2 6.53 2 12.06C2 17.06 5.66 21.21 10.44 21.96V14.96H7.9V12.06H10.44V9.85C10.44 7.34 11.93 5.96 14.22 5.96C15.31 5.96 16.45 6.15 16.45 6.15V8.62H15.19C13.95 8.62 13.56 9.39 13.56 10.18V12.06H16.34L15.89 14.96H13.56V21.96A10 10 0 0 0 22 12.06C22 6.53 17.5 2.04 12 2.04Z" />
                    </svg>
                    </div>
                </a>
              </div>
              <div class="social-items">
                <a
                  href="https://www.instagram.com/patagonia_blend/"
                  target="_blank"
                  rel="noopener"
                  aria-label="Instagram"
                  ><div class="icon-fa">
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                      <path fill="currentColor" d="M7.8,2H16.2C19.4,2 22,4.6 22,7.8V16.2A5.8,5.8 0 0,1 16.2,22H7.8C4.6,22 2,19.4 2,16.2V7.8A5.8,5.8 0 0,1 7.8,2M7.6,4A3.6,3.6 0 0,0 4,7.6V16.4C4,18.39 5.61,20 7.6,20H16.4A3.6,3.6 0 0,0 20,16.4V7.6C20,5.61 18.39,4 16.4,4H7.6M17.25,5.5A1.25,1.25 0 0,1 18.5,6.75A1.25,1.25 0 0,1 17.25,8A1.25,1.25 0 0,1 16,6.75A1.25,1.25 0 0,1 17.25,5.5M12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9Z" />
                  </svg>  
                  </div></a>
              </div>
            </div>
            <div class="social-footer" style="text-align: center">
              <h5
                style="color: white; font-weight: 700; letter-spacing: 1.5px"
              >
                Medios de Pago
              </h5>
              <div style="text-align: center">
                <div
                  style="
                    padding: 5px;
                    background-color: white;
                    width: 186px;
                    border-radius: 8px;
                    margin: 10px auto;
                  "
                >
                  <img
                    loading="lazy"
                    src="https://www.patagoniablend.cl/patagonia_blend/img/webpay.png"
                    alt="Medios de pago"
                    title="Medios de pago"
                    style="border-radius: 4px; width: 180px"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr class="line-footer" />
        <div class="bottom-footer">
          <div class="left-footer">
            <p>© 2019 Patagonia Blend</p>
          </div>
          <div class="right-footer">
            <p>
              <a href="https://www.patagoniablend.cl/nuestra-tostaduria/"
                >Nuestra tostaduría</a
              >
              &nbsp;/&nbsp;
              <a href="https://www.patagoniablend.cl/contacto/"
                >Contáctanos</a
              >
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>