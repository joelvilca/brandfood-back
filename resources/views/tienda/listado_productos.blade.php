                  <div
                    v-cloak
                    class="col-6 col-md-4"
                    v-for="(producto, index) in datos"
                    :key="index"
                    style="padding:0px 4px;"
                  >
                  
                      <div
                        class="shop-front-thumbs"
                        style="cursor: pointer; position: relative;"
                      >
                      <a v-bind:href="'{{URL::to('/')}}/producto/'+producto.slug" style="text-decoration: none !important;color:transparent !important;">
                        <div class="shop-thumb-img text-center">
                          <div>
                            <img loading="lazy" :src="producto.imagen_thumbnail_path" :alt="producto.nombre" :title="producto.nombre"/>
                          </div>
                        </div>
                      </a>
                        <hr class="shop" style="border-top:1px solid #ddd !important;" />
                        <div id="listado-icon" class="bottom-caption-right">
                          <v-btn icon aria-label="Tienda" @click="handleOpen(producto)">
                            <svg style="width:26px;height:26px" viewBox="0 0 24 24">
                              <path fill="currentColor" d="M19 20C19 21.11 18.11 22 17 22C15.89 22 15 21.1 15 20C15 18.89 15.89 18 17 18C18.11 18 19 18.9 19 20M7 18C5.89 18 5 18.89 5 20C5 21.1 5.89 22 7 22C8.11 22 9 21.11 9 20S8.11 18 7 18M7.2 14.63L7.17 14.75C7.17 14.89 7.28 15 7.42 15H19V17H7C5.89 17 5 16.1 5 15C5 14.65 5.09 14.32 5.24 14.04L6.6 11.59L3 4H1V2H4.27L5.21 4H20C20.55 4 21 4.45 21 5C21 5.17 20.95 5.34 20.88 5.5L17.3 11.97C16.96 12.58 16.3 13 15.55 13H8.1L7.2 14.63M8.5 11H10V9H7.56L8.5 11M11 9V11H14V9H11M14 8V6H11V8H14M17.11 9H15V11H16L17.11 9M18.78 6H15V8H17.67L18.78 6M6.14 6L7.08 8H10V6H6.14Z" />
                          </svg>
                            </v-btn>
                        </div>
                        <a v-bind:href="'{{URL::to('/')}}/producto/' + producto.slug" style="text-decoration: none !important;color:transparent !important;">
                        <div class="bottom-caption">              
                          <div class="top-caption">
                            <p class="categoria-producto" style="margin-bottom: 2px;">
                              @{{ producto.categoria.nombre }}
                            </p>
                            <h6 class="producto-nombre" style="cursor:pointer;">
                              @{{ producto.nombre }}
                            </h6>
                            <p style="color:rgba(0,0,0,.87); font-size:0.63em !important; margin-bottom:0; padding:0 10px">$@{{ formatMoney(producto.precio) }}</p>
                          </div>
                        </div>
                        </a>
                      </div>
                  </div>