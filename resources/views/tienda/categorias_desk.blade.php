
    <div v-cloak v-if="selectedCat.length > 0" class="filtro__check" style="float:right; padding-top:12px; cursor: pointer;" @click="getFilter('Quitar')">
      <svg style="width:24px;height:24px" viewBox="0 0 24 24" style="margin-top:2px; margin-right:5px;">
        <path fill="currentColor" d="M20 6.91L17.09 4L12 9.09L6.91 4L4 6.91L9.09 12L4 17.09L6.91 20L12 14.91L17.09 20L20 17.09L14.91 12L20 6.91Z" />
    </svg>
      Quitar filtros
    </div>
    <div class="sidebar-header" style="margin: 0;">
      <h5 style=" text-align: left; font-weight:600;font-size: 18px;">
        Categorías
      </h5>
    </div>
    <div class="inner-categories">
      <ul class="list-group">
        <li
          v-cloak
          v-for="(category, index) in categorias"
          :key="index"
          class="list-group-item"
          style="cursor: pointer"
        >   
            <input
              v-cloak
              type="checkbox"
              v-model="selectedCat"
              :value="category.id"
              style="cursor: pointer; margin-top:-5px; margin-right:8px;"
              @click="getFilter(category.id)"
            >
            <label @click="getFilter(category.id)" :for="category.nombre+category.id"  style="cursor: pointer;"> @{{ category.nombre }} (@{{category.numero_productos}})</label><br>
        </li>
      </ul>
    </div>