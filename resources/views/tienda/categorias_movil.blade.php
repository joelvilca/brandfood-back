<v-dialog
transition="dialog-top-transition"
v-model="catVisible"
scrollable
max-width="400"
:fullscreen="$vuetify.breakpoint.mobile"
>
<template v-slot:default="dialog">
  <v-card style="overflow-y: hidden;">
    <v-toolbar
      style="margin-bottom:15px; letter-spacing: 1.8px; font-size: 1.5rem; flex:unset;"
      color="grey lighten-2"
      text-align="center"
    ><v-spacer></v-spacer>Categorías<v-spacer></v-spacer>
    <v-btn
    icon
    color="black"
    @click="dialog.value = false"
    style="position:absolute; right:15px;"
  >
  <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
</svg>
  </v-btn></v-toolbar>
    <v-card-text style="padding:0 5%" class="align-self-center;">
        <div
                        class="inner-categories"
                        style="padding: 15px 0px; margin-left: 20px"
                      >
                        <ul class="list-group">
                          <li
                              v-for="(category, index) in categorias"
                              :key="index"
                              class="list-group-item"
                              style="cursor: pointer"
                            >   
                                <input
                                  type="checkbox"
                                  v-model="selectedCat"
                                  :value="category.id"
                                  :id="category.nombre+category.id"
                                  style="cursor: pointer"
                                  @click="getFilter(category.id)"
                                >
                                <label :for="category.nombre+category.id" style="cursor: pointer; margin-left:10px;margin-top:-4px;"> @{{ category.nombre }} (@{{category.numero_productos}})</label><br>
                            </li>
                        </ul>
                      </div>
    </v-card-text>
    <v-divider></v-divider>
    <v-card-actions class="justify-end">
        <v-row align="center">
            <v-col
              cols="12"
              sm="12"
              style="padding: 20px 16px 10px 16px !important;"
            >
              <div class="text-center">            
                <v-btn
                    color="info"
                    block
                    dark
                    x-large
                    @click="dialog.value = false"
                    
                >Aplicar filtros</v-btn>
                <v-btn
                text
                small
                @click="getFilter('Quitar')"
                >Limpiar filtros</v-btn>
              </div>
            </v-col>
        </v-row>
    </v-card-actions>
  </v-card>
</template>
</v-dialog>
