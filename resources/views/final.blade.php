<!DOCTYPE html>
<html lang="es-ES" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5"/>
        <title>Tienda Online | Patagonia Blend | Una taza con identidad</title>
        <meta name="description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta name="keywords" content="cafe de especialidad, te en hebras, infusiones, cafeteras, cafe verde">
        <meta name="author" content="Patagonia Blend">
        <link rel="shortcut icon" href="https://patagoniablend.cl/patagonia_blend/img/master/favicon.png">
        <meta property="og:title" content="Tienda Online de Patagonia Blend">
        <meta property="og:description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta property="og:image" content="https://patagoniablend.cl/patagonia_blend/img/master/logo-movil.png">
        <meta property="og:url" content="https://tienda.patagoniablend.cl">
        <meta property="og:site_name" content="Tienda Online de Patagonia Blend" />
        <meta property="twitter:title" content="Tienda Online de Patagonia Blend">
        <meta property="twitter:description" content="Te invitamos a conocer todos nuestros productos creados con un cariño especial desde la patagonia.">
        <meta property="twitter:image:src" content="https://patagoniablend.cl/patagonia_blend/img/master/logo-movil.png">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/style.css">
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">       
        <link
        href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css"
        rel="stylesheet"
        />
        @include('base.facebook')
        @include('base.hubspot')
        @include('base.google')
    </head>
  <body>
	<div id="app">
      <v-app>
        @include('base.toolbar')
        <v-container v-cloak>
				<div class="main">
                <div class="section" style="margin-top: 25px" id="step">
                  <div class="row">
                    <div class="col-md-7">
                      <v-card v-cloak class="mb-5" style="padding: 20px">
                        <div class="span-billing">
                          <h3>Estado de su Pedido</h3>
                          <hr
                            class="summary-line"
                            style="margin: 10px auto 20px auto !important"
                          />
                          <div
                            v-cloak
                            role="alert"
                            style="font-size: 0.9em !important; line-height: 24px"
                            v-if="compra.pago.estado_pago === 'PAGADO'"
                          >
                          <v-alert
                              type="success"
                            >Pago realizado exitosamente.</v-alert>
                            
                          </div>
                          <div
                            v-cloak
                            v-else
                            role="alert"
                            style="font-size: 0.9em !important; line-height: 24px"
                          >
                            <v-alert
                            type="error"
                          >
                          Estimado Cliente, le informamos que la transacción fue fallida,
                              si quiere puede volver para realizar el pago de su compra
                            presionando el botón "VOLVER PARA FINALIZAR COMPRA".</v-alert>
                            
                          </div>
                          <div class="customer-form">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>N° de Orden</label>
                                  <div v-cloak> @{{ compra.numero_orden }}</div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>N° de Boleta</label>
                                  <div v-cloak v-if="compra.pago.num_documento"> @{{ compra.pago.num_documento }}</div>
                                  <div v-cloak v-else> Pendiente</div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Forma de pago</label>
                                  <div> @{{ compra.pago.tipo }}</div>
                                </div>
                              </div>
                              <div class="col-md-6" v-if="compra.pago.tipo == 'WEBPAY'">
                                <div class="form-group">
                                  <label>Código de Autorización</label>
                                  <div> @{{ compra.pago.codigo_autorizacion }}</div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Email</label>
                                  <div> @{{ compra.cliente.email }}</div>
                                </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Nombre y Apellido</label>

                                  <div>@{{ compra.cliente.nombre }} @{{ compra.cliente.apellidos }} </div>
                                </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Teléfono</label>
                                  <div>@{{ compra.cliente.telefono }}</div>
                                </div>
                              </div>
                              
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Dirección y Ciudad</label>

                                  <div>@{{ compra.direccion }}, @{{ compra.ciudad }}</div>
                                </div>
                              </div>
                                                      

                              <div class="col-md-12" v-if="compra.notas">
                                <div class="form-group">
                                  <label>Notas al pedido</label>
                                  <div>@{{ compra.notas }}</div>
                                </div>
                              </div>

                            <div class="col-md-6" v-if="compra.courier.nombre == Chilexpress" >
                              <div class="form-group">
                                <label>Courier</label>

                                <div>@{{ compra.courier.nombre }}</div>
                              </div>
                            </div>
                            <div class="col-md-6" v-else >
                              <div class="form-group">
                                <label>Forma de entrega</label>

                                <div>@{{ compra.courier.nombre }}</div>
                              </div>
                            </div>

                            <div class="col-md-6" v-if="compra.num_seguimiento" >
                              <div class="form-group">
                                <label>N° de Seguimiento</label>
                                <div>@{{ compra.num_seguimiento }}</div>
                              </div>
                            </div>

                              <div class="col-md-6">
                                <a href="{{URL::to("/")}}/finalizar-compra">
                                <v-btn
                                  v-if="compra.pago.estado_pago != 'PAGADO'"
                                  color="primary"
                                  >Volver para Finalizar Compra</v-btn
                                >
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </v-card>
                    </div>
                    <div class="col-md-5">
                      <div class="order-summary" style="background-color: white">
                        <h5>Resumen del Pedido</h5>

                        <div class="order-details">
                          <hr class="summary-line" />
                          <div
                            class="inner-order col-md-12"
                            style="height: 50px"
                            v-for="(shop, index) in compra.detalles"
                            :key="index"
                          >
                            <div class="label-left">
                              <img
                                :src="shop.item_producto.imagen.imagen_thumbnail_path"
                                width="25"
                                height="25"
                              />
                              <p
                                style="font-size: 0.85em !important; font-weight: 600:margin-bottom:0"
                              >
                                @{{ shop.nombre_producto }}
                                <br />
                                <span
                                  style="font-size: 10px !important; font-weight: 600:margin-bottom:0"
                                  >@{{ shop.nombre_item }}</span
                                >
                              </p>
                            </div>
                            <div class="increment" style="">x @{{ shop.cantidad }}</div>
                            <div class="product-total">
                              <p>$ @{{ formatMoney(shop.subtotal) }}</p>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <table style="margin-bottom: 10px">
                            <tr>
                              <td style="width: 100px; font-weight: 700">SubTotal:</td>
                              <td style="width: 80px; font-weight: 700">
                                $@{{ formatMoney(compra.subtotal) }}
                              </td>
                            </tr>
                            <tr v-if="compra.cupon != null">
                              <td style="width: 100px">Descuento cupón:</td>
                              <td style="width: 80px">
                                $@{{ formatMoney(compra.monto_descuento) }}
                              </td>
                            </tr>
                            <tr>
                              <td style="width: 100px">Despacho:</td>
                              <td style="width: 80px">
                                $@{{ formatMoney(compra.precio_despacho) }}
                              </td>
                            </tr>
                            <tr>
                              <td style="width: 100px; font-weight: 700">Total:</td>
                              <td style="width: 80px; font-weight: 700">
                                $@{{ formatMoney(compra.precio_total) }}
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </v-container>
      </v-app>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
    
    <script src="{{URL::to('/')}}/js/axios.min.js"></script>
    <script src="{{URL::to('/')}}/js/fin_compra.js"></script>

  </body>
</html>
