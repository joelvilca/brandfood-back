           
                <v-container>
                    <v-row class="scrolling-wrapper">
                     @foreach($relacionados as $rel)
                     
                     
                        <v-card  id="relacionado-card" class="mx-auto">
                        <a href="{{URL::to('/')}}/producto/{{$rel->slug}}" style="text-decoration: none;">
                        <v-img
                            src="{{$rel->imagen_url}}"
                            class="align-center"

                        >
                            
                        </v-img>
                        
                        <v-card-subtitle>
                            {{$rel->cat}}
                          </v-card-subtitle>
                        <v-card-title style="font-weight: 700; color:black;">
                            {{$rel->nombre}}
                          </v-card-title>
                        </a>
                        </v-card>
                    @endforeach
                    </v-row>
                </v-container>