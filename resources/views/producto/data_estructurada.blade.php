<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Product",
        "productID": "{{ $producto->id }}",
        "name": "{{ $producto->nombre }}",
        "description": "{{ $producto->descripcion_larga }}",
        "url": "{{URL::to("/")}}/producto/{{ $producto->slug }}",
        "image": "{{$producto_img->imagen_url}}",
        "brand": "{{$producto_cat->nombre}}",
        "offers": 
          {
            "@type": "Offer",
            "price": "{{$producto->precio}}",
            "priceCurrency": "CLP",
            "itemCondition": "https://schema.org/NewCondition",
            "availability": "https://schema.org/InStock"
          },
        "additionalProperty": 
          {
            "@type": "PropertyValue",
            "propertyID": "{{$producto_cat->id}}",
            "value": "{{$producto_cat->nombre}}"
          }
    }
 </script>