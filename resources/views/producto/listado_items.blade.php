                  <v-list-item
                  v-cloak
                        id="lista-items"
                        v-for="(item, index) in items"
                        :key="index"
                        style="display: inline-flex; width:250px;"
                  >
                    <v-list-item-avatar style="height: 70px; min-width: 60px;width: 70px;border-radius: unset !important; margin-right:1rem;">
                      <v-img :src="item.imagen.imagen_thumbnail_path" style="cursor:pointer" @click="handleOpen(item)"></v-img>
                      <svg style="width:24px;height:24px;color:#112448; position:absolute; top:0; left:0;" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M13,9H11V7H13M13,17H11V11H13M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                    </svg>
                    </v-list-item-avatar>
          
                    <v-list-item-content>
                      <v-list-tile-action  style="max-width: 140px !important;">
                        <v-list-item-title v-html="item.nombre" style="font-weight: 400;margin-bottom:5px;text-overflow: ellipsis;"></v-list-item-title>
                        <v-list-item-subtitle v-cloak style="color:#212121; font-weight:600;font-style: normal;letter-spacing: 1px;margin-bottom:5px;
                        text-transform: uppercase;">$@{{ formatMoney(item.precio_bruto) }}</v-list-item-subtitle>
                      </v-list-tile-action>
                      <v-list-tile-action>
                        <div
                          v-if="item.stock_disponible >0"
                          class="product__quantity">
                          <button
                            :disabled="item.cantidad === 0"
                            @click="addCount(item, producto, 'less')"
                            class="product__quantityBtn  less">
                           - 
                          </button>
                          <input
                            v-model="item.cantidad"
                            style="width: 45px;"
                            class="product__quantityText">
                          <button
                            :disabled="item.cantidad === item.stock_disponible"
                            @click="addCount(item, producto, 'plus')"
                            class="product__quantityBtn plus">
                           + 
                          </button>
                        </div>
                        <div v-else>
                          <v-list-item-subtitle>No disponible</v-list-item-subtitle>
                        </div>
                      </v-list-tile-action>
                    </v-list-item-content>
                  </v-list-item>
                  @include('producto.popup_item')