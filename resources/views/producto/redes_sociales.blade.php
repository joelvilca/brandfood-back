        <!-- Redes Sociales -->
        <meta property="og:title" content="{{$producto->nombre}} | Tienda Online de Patagonia Blend">
        <meta property="og:description" content="{{$producto->descripcion_larga}}">

        <meta property="og:image" content="{{$producto_img->imagen_url}}">
        <meta property="og:url" content="{{URL::to("/")}}/producto/{{ $producto->slug }}">
        <meta property="og:site_name" content="Tienda Online de Patagonia Blend" />

        <meta property="twitter:title" content="{{$producto->nombre}} | Tienda Online de Patagonia Blend">
        <meta property="twitter:description" content="{{$producto->descripcion_larga}}">

        <meta property="twitter:image:src" content="{{$producto_img->imagen_url}}">